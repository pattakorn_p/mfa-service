// Get the modal
var modal01 = document.getElementById('signin');
var modal02 = document.getElementById('signup');
var modal03 = document.getElementById('addfund');
var modal04 = document.getElementById('editfund');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal01) {
        modal01.style.display = "none";
    }

    if (event.target == modal02) {
        modal02.style.display = "none";
    }

    if (event.target == modal03) {
        modal03.style.display = "none";
    }

    if (event.target == modal04) {
        modal04.style.display = "none";
    }
}

// Collapsible
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
  });
}