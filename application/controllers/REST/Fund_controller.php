<?php

    Class Fund_controller extends CI_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->model('Fetcher');
            $this->load->model('Fund');
        }

        public function fetchByAM($symbol){
            $this->Fetcher->fetchByAM($symbol);
        }

        public function fetchFundName(){

            /*foreach($this->Fetcher->fetchFundName() as $f){
                echo "Symbol: ".$f['fund_symbol']." Name: ".$f['fund_name']."<br>";
            }*/

            $this->Fetcher->fetchFundName();
        }
        public function fetchCatName(){

            $this->Fetcher->fetchCatName();
        }
        public function fetchFundPrice($symbol){

            //Default is five previous year

            $to_date = date("m-Y", strtotime("-5 year"));
            $from_date = date('m-Y');

            echo "Symbol: ".$symbol."<br>";

            echo "From date: ".$to_date." To date: ".$from_date."<br><br>";

            foreach($this->Fetcher->fetchFundHistoricPrice($symbol) as $f){
                echo "Date: ".$f['date']." NAV: ".$f['nav']." Change: ".$f['change']."<br>";
            }
        }

        public function fetchFundInfo($symbol){
            echo json_encode($this->Fetcher->fetchFundPresentPrice($symbol), JSON_UNESCAPED_UNICODE);
        }
        
        //Add funds to database
        public function addFundName(){
            ini_set('max_execution_time', 300);
            foreach($this->Fetcher->fetchFundName() as $f){
                $info = $this->Fetcher->fetchFundPresentPrice($f['fund_symbol']);
                $this->Fund->add($info);
            }

            echo 'Done.';
        }

        public function addAnnualReturn($symbol){
            
        }

        

    }

?>