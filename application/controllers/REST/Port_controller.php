<?php

    Class Port_controller extends CI_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->model('Fund');
            $this->load->model('User');
            $this->load->model('Fetcher');
            $this->load->model('Portfolio');
            $this->load->library('session');
            $this->load->helper('url');
        }

        public function getPortByUser(){
            $id;
            if($_SESSION['logged_in'] == TRUE){
                $id = $_SESSION['user_id'];
            }
            else{
                throw new Exception('Please log in first.');
            }
            $port = $this->Portfolio->getPortByUser($id);
            $result = array();

            foreach($port as $p){
                $fund = $this->Fund->getById($p['fund_id']);
                $price = $this->Fetcher->fetchFundPresentPrice($fund->fund_symbol);
                $result[] = [
                    'fund_name' => $fund->fund_symbol,
                    'amount' => $p['amount'],
                    'average_cost' => $p['average_cost'],
                    'market_price' => $price['nav'],
                    'sum_price' => $p['average_cost'] * $p['amount'],
                    'sum_market_price' => $price['nav'] * $p['amount'],
                    'profit-loss' => ($price['nav'] - $p['average_cost']) * $p['amount'],
                    'pl-percentage' => round(((($price['nav'] - $p['average_cost']) * $p['amount']) / ($p['average_cost'] * $p['amount'])) * 100, 2)
                ];
            }
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
        }

        public function addEntry(){

            $data['user_id'] = $_SESSION['user_id'];
            $fund = $this->Fund->getBySymbol($this->input->post('fund_code'));
            $data['fund_id'] = $fund->fund_id;
            
            $data['amount'] = $this->input->post('amount');
            $data['average_cost'] = $this->input->post('buy_price');

            $this->Portfolio->addEntry($data);
            
            redirect(base_url('view/port'), 'refresh');

        }

        public function addFavEntry(){
            
            $data['user_id'] = $_SESSION['user_id'];
            $fund = $this->Fund->getBySymbol($this->input->post('fund_name'));
            $data['fund_id'] = $fund->fund_id;

            $this->Portfolio->addFavEntry($data);
            
            redirect(base_url('view/favor'), 'refresh');

        }

        public function editEntry(){

            $data['user_id'] = $_SESSION['user_id'];
            $fund = $this->Fund->getBySymbol($this->input->post('fund_code'));

            $data['fund_id'] = $fund->fund_id;
            $data['amount'] = $this->input->post('amount');
            $data['average_cost'] = $this->input->post('buy_price');

            /*$data['fund_id'] = 2;
            $data['amount'] = 250;
            $data['average_cost'] = 12.5;*/

            $this->Portfolio->editEntry($data);

            redirect(base_url('view/port'), 'refresh');

        }

        public function delEntry(){

            $data['user_id'] = $_SESSION['user_id'];
            $data['fund_id'] = $this->Fund->getBySymbol($this->input->post('fund_name'))->fund_id;

            $this->Portfolio->delEntry($data);

            redirect(base_url('view/port'), 'refresh');

        }

        public function delFavEntry(){

            $data['user_id'] = $_SESSION['user_id'];
            $data['fund_id'] = $this->Fund->getBySymbol($this->input->post('fund_name'))->fund_id;

            $this->Portfolio->delFavEntry($data);

            redirect(base_url('view/favor'), 'refresh');

        }
        

    }

?>