<?php

    ini_set('max_execution_time', 500000);
    Class Stat_controller extends CI_Controller{

        public function __construct(){ 
            parent::__construct();
            $this->load->library('stats');
        }

        public function getQuarterChange($symbol){
            echo $this->stats->getQuarterChange($symbol);
        }

        public function getHalfYearChange($symbol){
            echo $this->stats->getHalfYearChange($symbol);
        }

        public function getAnnualChange($symbol){
            echo $this->stats->getAnnualChange($symbol);
        }

        public function get3YChange($symbol){
            echo $this->stats->get3YChange($symbol);
        }

        public function get2YChange($symbol){
            echo $this->stats->get2YChange($symbol);
        }

        public function predict($symbol){
            $this->stats->predict($symbol);
        }

        public function planner(){
            //$this->stats->getAAR($symbol = 'TMBABF');
            $this->stats->planner($init = 53169, $target = 100000, $duration = 25, $risk = 1, $am = ['TMBAM', 'BBLAM']);

        }

        public function insertAAR(){
            $this->stats->insertAAR();
        }
    }

?>