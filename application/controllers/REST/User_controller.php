<?php

    Class User_controller extends CI_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->model('User');
            $this->load->model('Fetcher');
            $this->load->model('Fund');
            $this->load->library('session');
            $this->load->helper('url');
        }

        public function register(){
            
            $username = $this->input->post('uname');
            $password = $this->input->post('psw');
            $email = $this->input->post('email');

            $data = ["username"=>$username, "password"=>$password,"email"=>$email];

            $this->User->register($data);

            redirect(base_url('view/index'), 'refresh');
        }

        public function login(){

            $cred = ['username'=>$this->input->post('uname'), 'password'=>$this->input->post('psw')];
            $sesion_data;
            $result = $this->User->login($cred);
            if($result){
                $session_data = [
                    'user_id' => $result->user_id,
                    'username' => $cred['username'],
                    'logged_in' => TRUE
                ];

                $this->session->set_userdata($session_data);
                
                redirect(base_url('view/index'), 'refresh');
            }





        }

        public function logout(){
            $sess_array = array(
                'user_id' => '',
                'username' => '',
                'logged_in' => FALSE
            );
            $this->session->unset_userdata('logged_in', $sess_array);

            $this->session->sess_destroy();

            redirect(base_url('view/index'), 'refresh');
        }

        public function login_validation(){
            echo json_encode($this->User->login_validation($this->input->post('uname'), $this->input->post('psw')), JSON_UNESCAPED_UNICODE);
        }

        public function uname_available(){
            echo json_encode($this->User->uname_available($this->input->post('uname')), JSON_UNESCAPED_UNICODE);
        }

        public function email_available(){
            echo json_encode($this->User->email_available($this->input->post('email')), JSON_UNESCAPED_UNICODE);
        }
    }

?>