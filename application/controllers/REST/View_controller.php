<?php
    Class View_controller extends CI_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->model('Fetcher');
            $this->load->model('Fund');
            
            $this->load->model('Portfolio');
            $this->load->library('session');
            $this->load->library('stats');
        }

        public function index(){
            
            $this->load->helper('url');
            $this->load->view('index.php');

        }

        public function detail($id){
            $this->load->model('Fetcher');

            //var_dump($id->fund_id);
            //die();
            $fund = $this->Fund->getById($id);

            $data = $this->Fetcher->fetchFundPresentPrice($fund->fund_symbol);
            $nav_data = $this->Fetcher->fetchFundAnnualPrice($fund->fund_symbol);

            $historic_price = array();
            $historic_date = array();

            foreach($nav_data as $n){
                $historic_price[] = $n['nav']; 
                $historic_date[] = $n['date'];
            }
            
            $historic_price = array_reverse($historic_price);
            $historic_date = array_reverse($historic_date);

            $data['fund_am_symbol'] = $fund->fund_am_symbol;
            $data['historic_price'] = $historic_price;
            $data['historic_date'] = $historic_date;
            $data['fund_risk'] = $fund->fund_risk;
            
            $this->load->helper('url');
            $this->load->view('fundinfo.php', $data);
        }

        public function port(){
            
            $id;
            if($_SESSION['logged_in'] == TRUE){
                $id = $_SESSION['user_id'];
            }
            else{
                throw new Exception('Please log in first.');
            }
            $port = $this->Portfolio->getPortByUser($id);
            $result = array();

            foreach($port as $p){
                $fund = $this->Fund->getById($p['fund_id']);
                $price = $this->Fetcher->fetchFundPresentPrice($fund->fund_symbol);
                $result[] = [
                    'fund_id' => $fund->fund_id,
                    'fund_name' => $fund->fund_symbol,
                    'amount' => $p['amount'],
                    'average_cost' => $p['average_cost'],
                    'market_price' => $price['nav'],
                    'sum_price' => round($p['average_cost'] * $p['amount'], 2),
                    'sum_market_price' => round($price['nav'] * $p['amount'], 2),
                    'profit-loss' => round(($price['nav'] - $p['average_cost']) * $p['amount'], 2),
                    'pl-percentage' => round(((($price['nav'] - $p['average_cost']) * $p['amount']) / ($p['average_cost'] * $p['amount'])) * 100, 2)
                ];
            }
            //echo json_encode($result, JSON_UNESCAPED_UNICODE);

            $this->load->helper('url');
            $payload['data'] = $result;
            $payload['name'] = $this->Fetcher->fetchCatName();

            $this->load->view('portfolio.php', $payload);

        }
        
        public function favorite(){
            
            $id;
            if($_SESSION['logged_in'] == TRUE){
                $id = $_SESSION['user_id'];
            }
            else{
                throw new Exception('Please log in first.');
            }
            $fav = $this->Portfolio->getFavByUser($id);
            $result = array();

            foreach($fav as $f){
                $fund = $this->Fund->getById($f['fund_id']);
                $price = $this->Fetcher->fetchFundPresentPrice($fund->fund_symbol);
                if(is_numeric($price['change'])){

                    $result[] = [
                        'fund_id' => $fund->fund_id,
                        'fund_name' => $fund->fund_symbol,
                        'nav' => $price['nav'],
                        'date' => $price['date'],
                        'change' => $price['change'],
                        '%change' => number_format(($price['change'] / $price['nav']) * 100, 4, '.', '')
                    ];
                }
                else{

                    $result[] = [
                        'fund_id' => $fund->fund_id,
                        'fund_name' => $fund->fund_symbol,
                        'nav' => $price['nav'],
                        'date' => $price['date'],
                        'change' => $price['change'],
                        '%change' => "N/A"
                    ];
                }
            }
            //echo json_encode($result, JSON_UNESCAPED_UNICODE);

            $this->load->helper('url');
            $payload['data'] = $result;
            $payload['name'] = $this->Fetcher->fetchCatName();

            $this->load->view('favouritefund.php', $payload);

        }

        public function fundlist($am){
            $this->load->helper('url');
            $data['am'] = $am;
            $data['funds'] = $this->Fetcher->fetchByAM($am);

            $this->load->view('fundlist.php', $data);
        }

        public function compare(){

            $name['name'] = $this->Fetcher->fetchCatName();
            $this->load->helper('url');
            $this->load->view('comparing.php', $name);
        }

        public function compared(){

            $data = array();

            $data['duration'] = $this->input->post('duration');
            $data['amount'] = $this->input->post('fund_amount');
            $data['initial'] = $this->input->post('initial');

            $i = 1;
            
            while($i <= $data['amount']){
                
                $data['funds'][] = $this->input->post('fund'.$i);

                $i++;
            }

            /*foreach($data['funds'] as $f){
                $data['funds'][$f]['perf'] = $this->stats->perfCompare($data['duration'], $data['initial'], )
            }*/

            $result = $this->stats->perfCompare($this->input->post('duration'), $this->input->post('initial'), $data['funds']);

            /*var_dump($result);
            die();*/

            $data['result'] = $result;

            $this->load->helper('url');
            $this->load->view('compfund.php', $data);
        }

        public function prediction(){

            $name['name'] = $this->Fetcher->fetchCatName();
            
            $this->load->helper('url');
            $this->load->view('prediction.php', $name);

        }

        public function predicted(){

            $data = array();
            $fund = array();
            $data['amount'] = $this->input->post('fund_amount');

            $i = 1;

            while($i <= $data['amount']){
                
                $data['funds'][] = $this->stats->predict($this->input->post('fund'.$i));
                $data['info'][] = $this->Fund->getBySymbol($this->input->post('fund'.$i));
                $data['nav'][] = $this->Fetcher->fetchFundPresentPrice($this->input->post('fund'.$i));

                $i++;
            }

            //var_dump($data['funds']);
            //die();

            $this->load->helper('url');
            $this->load->view('predictfund.php', $data);
        }

        public function planning(){
            
            $this->load->helper('url');
            $this->load->view('planning.php');

        }

        public function planned(){

            $data['initial'] = $this->input->post('initial');
            $data['target'] = $this->input->post('target');
            $data['duration'] = $this->input->post('year');
            $data['risk'] = $this->input->post('risk');
            $data['am'] = $this->input->post('fund_firm[]');

            //var_dump($this->input->post('fund_firm[]'));
            //die();

            //var_dump($data);
            //die();

            $funds = $this->stats->planner($data['initial'], $data['target'], $data['duration'], $data['risk'], $data['am']);

            //var_dump($funds);
            //die();

            $this->load->helper('url');
            $this->load->view('planningfund.php', $funds);

        }

    }

?>