<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds Assistant</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">

    <script src="<?php echo base_url('assets/js/script.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

</head>
<body>
    
    <div class="navbar">
        <div class="navcon">
            <a href="<?php echo base_url('view/index') ?>" class="logo" style="color: #2d6da3;">MFA</a>
            <a href="<?php echo base_url('view/compare')?>" class="navrbd">Comparing</a>
            <a href="<?php echo base_url('view/predict_form')?>" class="nav">Estimation</a>
            <a href="<?php echo base_url('view/plan_form') ?>" class="nav">Planning</a>
            <?php if(isset($_SESSION['username'])) { ?>
                <a href="<?php echo base_url('view/favor') ?>" class="nav">Favourite</a>
                <a href="<?php echo base_url('view/port') ?>" class="nav">Portfolio</a>
            <?php } ?>
            <?php if (isset($_SESSION['username'])) { ?>
                <div class="navsignout">
                    <span style="margin-right: 10px;"><?php echo $_SESSION['username'] ?></span>
                    <a href="<?php echo base_url('logout') ?>" style="color: #2d6da3; cursor: pointer; text-decoration: none;">Sign Out</a>
                </div>
            <?php } else { ?>
                <div class="navsignin" onclick="document.getElementById('signin').style.display='block'" style="width:auto; color: #2d6da3;">Sign In</div>
            <?php } ?>
        </div>
    </div>

    <div id="signin" class="modal">
  
        <form id="signinForm" class="modal-content animate" action="<?php echo base_url('login') ?>" method="POST">
            <div class="logocontainer">
                <a href="<?php echo base_url('view/index') ?>" style="text-decoration: none; color: #2d6da3;"><h1>MFA</h1></a>
                <span onclick="document.getElementById('signin').style.display='none'" class="close" title="Close">&times;</span>
            </div>
      
            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" id="uname_in" required>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_in" required>
                
                <div class="error_text"><span></span></div>
              
                <button type="submit">Login</button>
            </div>
      
            <div class="container" style="background-color:#f1f1f1">
                <span class="signup" onclick="document.getElementById('signup').style.display='block' ,
                document.getElementById('signin').style.display='none'" style="width:auto;">Sign Up</span>
            </div>
        </form>
    </div>

    <div id="signup" class="modal">

        <form id="signupForm" class="modal-content animate" action="<?php echo base_url('register') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Sign Up</h1>
                <span onclick="document.getElementById('signup').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <div class="error_text_uname"><span></span></div>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_up" required>
                <div class="error_text_psw"><span></span></div>

                <label for="psw"><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Confirm Password" name="psw1" required>
                <div class="error_text_psw1"><span></span></div>
                
                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email" required>
                <div class="error_text_email"><span></span></div>

                <button type="submit">Create Account</button>
            </div>
        </form>
            
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">
                        
                        <div class="listheader" style="margin-bottom: 20px; margin-top: 20px;">
                            <h1>Mutual Fund Firm List</h1>
                        </div>

                        <div class="listcon">
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/ktam.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/KTAM') ?>" style="color: #2d6da3; text-decoration: none;">KTAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน กรุงไทย จำกัด (มหาชน)
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/krungsriasset.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/KrungsriAsset') ?>" style="color: #2d6da3; text-decoration: none;">KrungsriAsset</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน กรุงศรี จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/kasset.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/KAsset') ?>" style="color: #2d6da3; text-decoration: none;">KAsset</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน กสิกรไทย จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/cimbprincipal.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/CIMBPrincipal') ?>" style="color: #2d6da3; text-decoration: none;">CIMBPrincipal</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ซีไอเอ็มบี-พรินซิเพิล จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/solarisfund.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/SolarisFund') ?>" style="color: #2d6da3; text-decoration: none;">SolarisFund</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน โซลาริส จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/tmbam.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/TMBAM') ?>" style="color: #2d6da3; text-decoration: none;">TMBAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ทหารไทย จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/talisfund.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/TALISAM') ?>" style="color: #2d6da3; text-decoration: none;">TALISAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ทาลิส จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/tiscoam.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/TISCOAM') ?>" style="color: #2d6da3; text-decoration: none;">TISCOAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ทิสโก้ จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/scbam.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/SCBAM') ?>" style="color: #2d6da3; text-decoration: none;">SCBAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ไทยพาณิชย์ จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/tfund.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/TFUND') ?>" style="color: #2d6da3; text-decoration: none;">TFUND</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ธนชาต จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/bcap.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/BCAP') ?>" style="color: #2d6da3; text-decoration: none;">BCAP</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน บางกอกแคปปิตอล จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/phatra.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/PhatraAM') ?>" style="color: #2d6da3; text-decoration: none;">PhatraAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ภัทร จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/uobam.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/UOBAM') ?>" style="color: #2d6da3; text-decoration: none;">UOBAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ยูโอบี (ประเทศไทย) จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/lhfund.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/LHFUND') ?>" style="color: #2d6da3; text-decoration: none;">LHFUND</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน แลนด์ แอนด์ เฮ้าส์ จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/oneam.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/ONEAM') ?>" style="color: #2d6da3; text-decoration: none;">ONEAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน วรรณ จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/skfm.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/SKFM') ?>" style="color: #2d6da3; text-decoration: none;">SKFM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน สยามไนท์ ฟันแมเนจเม้นท์ จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/aberdeen.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/ABERDEEN') ?>" style="color: #2d6da3; text-decoration: none;">ABERDEEN</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน อเบอร์ดีน จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/mfc.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/MFC') ?>" style="color: #2d6da3; text-decoration: none;">MFC</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน เอ็มเอฟซี จำกัด (มหาชน)
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/assetfund.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/ASSETFUND') ?>" style="color: #2d6da3; text-decoration: none;">ASSETFUND</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน แอสเซท พลัส จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/bblam.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/BBLAM') ?>" style="color: #2d6da3; text-decoration: none;">BBLAM</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน บัวหลวง จำกัด
                                    </div>
                                </div>
                            </div>
                            <div class="firmcon_bot">
                                <div class="firmpic">
                                    <img src="<?php echo base_url('assets/fund_logo/pamc.gif');?>">
                                </div>
                                <div class="firmname">
                                    <div class="short">
                                        <a href="<?php echo base_url('view/fundlist/PAMC') ?>" style="color: #2d6da3; text-decoration: none;">PAMC</a>
                                    </div>
                                    <div class="full">
                                        บริษัทหลักทรัพย์จัดการกองทุน ฟิลลิป จำกัด
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

    <script>

    
        $("#signinForm").validate({   

            onkeyup: false,
            onclick: false,
            onfocusout: false,

            rules:{
                psw:{
                    remote:{
                        type: 'post',
                        url: '<?php echo base_url('view/login_validation') ?>',
                        data: {
                            uname: function(){
                                return $("#uname_in").val();
                            }
                        },
                        dataType: 'json'
                    }
                }
            },
            messages:{
                psw:"Incorrect Username or Password"
            },
            errorPlacement: function(error, element) {
                error.appendTo(".error_text span");
            }
        });

        $("#signupForm").validate({
            rules:{
                uname:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12],
                    remote:{
                        type:'post',
                        url:'<?php echo base_url('view/uname_available') ?>'
                    }
                },
                psw:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12]
                },
                psw1:{
                    equalTo: "#psw_up"
                },
                email:{
                    required:true,
                    email:true,
                    remote:{
                        type:'post',
                        url:'<?php echo base_url('view/email_available') ?>'
                    }
                }
            },
            messages:{
                uname:"Username already taken.",
                email:"Email already in used."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "uname" )
                    error.appendTo(".error_text_uname span");
                else if  (element.attr("name") == "psw" )
                    error.appendTo(".error_text_psw span");
                else if (element.attr("name") == "psw1" )
                    error.appendTo(".error_text_psw1 span");
                else 
                    error.appendTo(".error_text_email span");
            }
        });

    </script>

</body>
</html>