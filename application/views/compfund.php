<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds Assistant</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">

    <script src="<?php echo base_url('assets/js/script.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/jquery-form/form@4.2.2/dist/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>
<body>
    
    <div class="navbar">
        <div class="navcon">
            <a href="<?php echo base_url('view/index') ?>" class="logo" style="color: #2d6da3;">MFA</a>
            <a href="<?php echo base_url('view/compare')?>" class="navrbd">Comparing</a>
            <a href="<?php echo base_url('view/predict_form')?>" class="nav">Estimation</a>
            <a href="<?php echo base_url('view/plan_form') ?>" class="nav">Planning</a>
            <?php if(isset($_SESSION['username'])) { ?>
                <a href="<?php echo base_url('view/favor') ?>" class="nav">Favourite</a>
                <a href="<?php echo base_url('view/port') ?>" class="nav">Portfolio</a>
            <?php } ?>
            <?php if (isset($_SESSION['username'])) { ?>
                <div class="navsignout">
                    <span style="margin-right: 10px;"><?php echo $_SESSION['username'] ?></span>
                    <a href="<?php echo base_url('logout') ?>" style="color: #2d6da3; cursor: pointer; text-decoration: none;">Sign Out</a>
                </div>
            <?php } else { ?>
                <div class="navsignin" onclick="document.getElementById('signin').style.display='block'" style="width:auto; color: #2d6da3;">Sign In</div>
            <?php } ?>
        </div>
    </div>

    <div id="signin" class="modal">
  
        <form id="signinForm" class="modal-content animate" action="<?php echo base_url('login') ?>" method="POST">
            <div class="logocontainer">
                <a href="<?php echo base_url('view/index') ?>" style="text-decoration: none; color: #2d6da3;"><h1>MFA</h1></a>
                <span onclick="document.getElementById('signin').style.display='none'" class="close" title="Close">&times;</span>
            </div>
      
            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" id="uname_in" required>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_in" required>

                <div class="error_text"><span></span></div>
              
                <button type="submit">Login</button>
            </div>
      
            <div class="container" style="background-color:#f1f1f1">
                <span class="signup" onclick="document.getElementById('signup').style.display='block' ,
                document.getElementById('signin').style.display='none'" style="width:auto;">Sign Up</span>
            </div>
        </form>
    </div>

    <div id="signup" class="modal">

        <form id="signupForm" class="modal-content animate" action="<?php echo base_url('register') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Sign Up</h1>
                <span onclick="document.getElementById('signup').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <div class="error_text_uname"><span></span></div>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_up" required>
                <div class="error_text_psw"><span></span></div>

                <label for="psw"><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Confirm Password" name="psw1" required>
                <div class="error_text_psw1"><span></span></div>
                
                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email" required>
                <div class="error_text_email"><span></span></div>

                <button type="submit">Create Account</button>
            </div>
        </form>
            
    </div>

    <div id="addfund" class="modal">

        <form class="modal-content animate" action="<?php echo base_url('port_add') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Add to Portfolio</h1>
                <span onclick="document.getElementById('addfund').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="fund_code"><b>Fund Code</b></label>
                <input type="text" placeholder="Enter Fund Code" name="fund_code" id="fund_code" readonly>

                <label for="owned_units"><b>Owned Units</b></label>
                <input type="text" placeholder="Enter Owned Units" name="amount" required>

                <label for="buy_price"><b>Buy Price</b></label>
                <input type="text" placeholder="Enter Buy Price" name="buy_price" required>

                <button type="submit">Add</button>
            </div>
        </form>
        
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">
                        <div class="header" style="margin-bottom: 20px; margin-top: 20px;">
                            <h1>Comparing Result</h1>
                        </div>

                        <div class="graphcon">
                            <canvas id="chart"></canvas>
                        </div>

                        <div class="investinfocon" style="margin-bottom: 20px; margin-top: 20px;">
                            <div class="initvalue" style="margin-bottom: 10px;">
                                <p style="font-size: 16px;">Initial Investment Value: <?php echo $initial; ?> ฿</p>
                            </div>
                            <table id="compfund">
                                <thead>
                                    <tr>
                                        <th>Fund</th>
                                        <th>Final Investment Value</th>
                                        <th>Change</th>
                                        <?php if(isset($_SESSION['username'])) { ?>
                                            <th></th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($funds as $f){ ?>
                                        <tr>
                                            <td><?php echo $f; ?></td>
                                            <?php if ( $result[$f]['result'] < $initial ) { ?>
                                                <td style="color: red;"><?php echo round($result[$f]['result'], 2); ?> ฿</td>
                                            <?php } else { ?>
                                                <td style="color: green;"><?php echo round($result[$f]['result'], 2); ?> ฿</td>
                                            <?php } ?>
                                            <?php if ( $result[$f]['result'] - $initial < 0 ) { ?>
                                                <td style="color: red;"><?php echo round($result[$f]['result'] - $initial, 2); ?> ฿</td>
                                            <?php } else { ?>
                                                <td style="color: green;"><?php echo round($result[$f]['result'] - $initial, 2); ?> ฿</td>
                                            <?php } ?>
                                            <?php if(isset($_SESSION['username'])){ ?>
                                            <td style="width: 80px;">
                                                <div class="toolscon">
                                                    <span class="add_fav" style="float: left; cursor: pointer; margin-right: 20px;">
                                                        <form onclick="ajaxFavAdd(this)" method="POST" name="add_fav_<?php echo $f; ?>">
                                                            <input type="hidden" name="fund_name" value="<?php echo $f; ?>">
                                                            <i class="far fa-star"></i>
                                                        </form>
                                                    </span>
                                                    <span class="add_port" onclick="document.getElementById('addfund').style.display='block',
                                                        document.getElementById('fund_code').value='<?php echo $f; ?>'"
                                                        style="float: left; cursor: pointer;">
                                                            <i class="fas fa-file-import"></i>
                                                    </span>
                                                </div>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

    <script>
        var ctx = document.getElementById("chart").getContext('2d');
        var funds = <?php echo json_encode($funds) ?>;
        var result = <?php echo json_encode($result) ?>;

        var data = new Array();;
        var i;

        var color = new Array();;

        for(i = 0; i < funds.length; ++i){
            data.push(result[funds[i]]['change']*100.00);
            color.push(getRandomColorHex());
        }
        console.log(data);
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                //x-axis mutual fund x
                labels: funds,
                //y-axis mutual fund annual by 3/6/12/24/36 months
                datasets: [{
                    label: 'Change in %',
                    backgroundColor: color,
                    //borderColor: ['red','blue','green'],
                    //backgroundColor: ['pink','lightblue','lightgreen'],
                    //data: [ result[funds[0]]['change']*100.00, result[funds[1]]['change']*100.00, result[funds[2]]['change']*100.00 ],
                    data: data,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:false
                        }
                    }]
                }
            }

        });

        /**
         * function to generate random color in hex form
         */
        function getRandomColorHex() {
            var hex = "0123456789ABCDEF",
                color = "#";
            for (var i = 1; i <= 6; i++) {
            color += hex[Math.floor(Math.random() * 16)];
            }
            return color;
        }
        
        
        $("#addForm").validate({

            rules:{
                amount:{
                    required:true,
                    number:true,
                    min:1
                },
                buy_price:{
                    required:true,
                    number:true,
                    min:1
                }
            }

        });
        
        $("#signinForm").validate({   

            onkeyup: false,
            onclick: false,
            onfocusout: false,

            rules:{
                psw:{
                    remote:{
                        type: 'post',
                        url: 'login_validation',
                        data: {
                            uname: function(){
                                return $("#uname_in").val();
                            }
                        },
                        dataType: 'json'
                    }
                }
            },
            messages:{
                psw:"Incorrect Username or Password"
            },
            errorPlacement: function(error, element) {
                error.appendTo(".error_text span");
            }
        });

        $("#signupForm").validate({
            rules:{
                uname:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12],
                    remote:{
                        type:'post',
                        url:'uname_available'
                    }
                },
                psw:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12]
                },
                psw1:{
                    equalTo: "#psw_up"
                },
                email:{
                    required:true,
                    email:true,
                    remote:{
                        type:'post',
                        url:'email_available'
                    }
                }
            },
            messages:{
                uname:"Username already taken.",
                email:"Email already in used."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "uname" )
                    error.appendTo(".error_text_uname span");
                else if  (element.attr("name") == "psw" )
                    error.appendTo(".error_text_psw span");
                else if (element.attr("name") == "psw1" )
                    error.appendTo(".error_text_psw1 span");
                else 
                    error.appendTo(".error_text_email span");
            }
        });

        var base_url = "<?php echo base_url() ?>";
        function ajaxFavAdd(el){
            $(el).ajaxSubmit({url:base_url+'fav_add', type:'post'});
            alert("Fund added to Favorite");
        }
    </script>
    
</body>
</html>