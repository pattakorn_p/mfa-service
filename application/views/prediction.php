<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds Assistant</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">

    <script src="<?php echo base_url('assets/js/script.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.default.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>
<body>
    
    <div class="navbar">
        <div class="navcon">
            <a href="<?php echo base_url('view/index') ?>" class="logo" style="color: #2d6da3;">MFA</a>
            <a href="<?php echo base_url('view/compare')?>" class="navrbd">Comparing</a>
            <a href="<?php echo base_url('view/predict_form')?>" class="nav">Estimation</a>
            <a href="<?php echo base_url('view/plan_form') ?>" class="nav">Planning</a>
            <?php if(isset($_SESSION['username'])) { ?>
                <a href="<?php echo base_url('view/favor') ?>" class="nav">Favourite</a>
                <a href="<?php echo base_url('view/port') ?>" class="nav">Portfolio</a>
            <?php } ?>
            <?php if (isset($_SESSION['username'])) { ?>
                <div class="navsignout">
                    <span style="margin-right: 10px;"><?php echo $_SESSION['username'] ?></span>
                    <a href="<?php echo base_url('logout') ?>" style="color: #2d6da3; cursor: pointer; text-decoration: none;">Sign Out</a>
                </div>
            <?php } else { ?>
                <div class="navsignin" onclick="document.getElementById('signin').style.display='block'" style="width:auto; color: #2d6da3;">Sign In</div>
            <?php } ?>
        </div>
    </div>

    <div id="signin" class="modal">
  
        <form id="signinForm" class="modal-content animate" action="<?php echo base_url('login') ?>" method="POST">
            <div class="logocontainer">
                <a href="<?php echo base_url('view/index') ?>" style="text-decoration: none; color: #2d6da3;"><h1>MFA</h1></a>
                <span onclick="document.getElementById('signin').style.display='none'" class="close" title="Close">&times;</span>
            </div>
      
            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" id="uname_in" required>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_in" required>

                <div class="error_text"><span></span></div>
              
                <button type="submit">Login</button>
            </div>
      
            <div class="container" style="background-color:#f1f1f1">
                <span class="signup" onclick="document.getElementById('signup').style.display='block' ,
                document.getElementById('signin').style.display='none'" style="width:auto;">Sign Up</span>
            </div>
        </form>
    </div>

    <div id="signup" class="modal">

        <form id="signupForm" class="modal-content animate" action="<?php echo base_url('register') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Sign Up</h1>
                <span onclick="document.getElementById('signup').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <div class="error_text_uname"><span></span></div>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_up" required>
                <div class="error_text_psw"><span></span></div>

                <label for="psw"><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Confirm Password" name="psw1" required>
                <div class="error_text_psw1"><span></span></div>
                
                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email" required>
                <div class="error_text_email"><span></span></div>

                <button type="submit">Create Account</button>
            </div>
        </form>
            
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">
                        
                        <div class="header" style="margin-bottom: 20px; margin-top: 20px;">
                            <h1>Mutual Fund Estimation</h1>
                        </div>

                        <form class="predict" id="predictForm" action="<?php echo base_url('view/predicted') ?>" method="POST">
                            <div class="container">

                                <div class="addfundcon" style="align-items: center; float: right;">
                                    <div id="add_fund" style="cursor: pointer;"><i class="fas fa-plus"></i> Add fund</div>
                                </div>

                                <div class="inputcon">
                                    <div class="inputgroup1">
                                        <label for="fund1"><b>Fund 1</b></label>
                                        <select name="fund1" id="fund1" required>
                                            <option value="" disabled selected>Select fund to estimate</option>
                                            <?php foreach($name as $n=>$val){ ?>
                                                <optgroup label="<?php echo $n; ?>">
                                                    <?php foreach($val as $v=>$tval){ ?>
                                                    <optgroup label="<?php echo $v; ?>">
                                                        <?php foreach($tval as $t){ ?>
                                                            <option value="<?php echo $t->fund_symbol; ?>"><?php echo $t->fund_symbol." - ".$t->fund_name; ?></option>
                                                        <?php } ?>
                                                    </optgroup>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" name="fund_amount" value="1">
                                    </div>
                                </div>

                                <button type="submit" style="width: 30%;">Next</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

    <script>
        var fund_amount = 1;
        
        $(document).ready(function() {
			$("#add_fund").click(function() {
                
                console.log(fund_amount);
                fund_amount++;
                $(".inputcon").append(`<div class="inputgroup` + fund_amount + `">
                                        <div class="delfundcon" style="align-items: center; float: right;">
                                            <div id="del_fund` + fund_amount + `" style="cursor: pointer;"><i class="fas fa-minus"></i> Delete fund</div>
                                        </div>
                                        <label for="fund` + fund_amount + `"><b>Fund ` + fund_amount + `</b></label>
                                        <select name="fund` + fund_amount + `" id="fund` + fund_amount + `" required>
                                            <option value="" disabled selected>Select fund to predict</option>
                                            <?php foreach($name as $n=>$val){ ?>
                                                <optgroup label="<?php echo $n; ?>">
                                                    <?php foreach($val as $v=>$tval){ ?>
                                                    <optgroup label="<?php echo $v; ?>">
                                                        <?php foreach($tval as $t){ ?>
                                                            <option value="<?php echo $t->fund_symbol; ?>"><?php echo $t->fund_symbol." - ".$t->fund_name; ?></option>
                                                        <?php } ?>
                                                    </optgroup>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" name="fund_amount" value=`+fund_amount+`>
                                    </div>`);
                                                        
                $("#fund"+fund_amount).selectize({
                    sortField: 'text'
                });
                $("#del_fund" + fund_amount).click(function() {
                    $(".inputgroup" + fund_amount).remove();
                    fund_amount--;
                });
            });
		});

        $("#predictForm").validate({
            rules:{
                fund1:{
                    required:true
                }
            }
        });

        $("#fund1").selectize({
            sortField: 'text',
            searchField: ['text', 'optgroup']
        });
        
        $("#signinForm").validate({   

            onkeyup: false,
            onclick: false,
            onfocusout: false,

            rules:{
                psw:{
                    remote:{
                        type: 'post',
                        url: 'login_validation',
                        data: {
                            uname: function(){
                                return $("#uname_in").val();
                            }
                        },
                        dataType: 'json'
                    }
                }
            },
            messages:{
                psw:"Incorrect Username or Password"
            },
            errorPlacement: function(error, element) {
                error.appendTo(".error_text span");
            }
        });

        $("#signupForm").validate({
            rules:{
                uname:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12],
                    remote:{
                        type:'post',
                        url:'uname_available'
                    }
                },
                psw:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12]
                },
                psw1:{
                    equalTo: "#psw_up"
                },
                email:{
                    required:true,
                    email:true,
                    remote:{
                        type:'post',
                        url:'email_available'
                    }
                }
            },
            messages:{
                uname:"Username already taken.",
                email:"Email already in used."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "uname" )
                    error.appendTo(".error_text_uname span");
                else if  (element.attr("name") == "psw" )
                    error.appendTo(".error_text_psw span");
                else if (element.attr("name") == "psw1" )
                    error.appendTo(".error_text_psw1 span");
                else 
                    error.appendTo(".error_text_email span");
            }
        });
    </script>

</body>
</html>