<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds Assistant</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">

    <script src="<?php echo base_url('assets/js/script.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.default.css">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>
<body>
    
    <div class="navbar">
        <div class="navcon">
            <a href="<?php echo base_url('view/index') ?>" class="logo" style="color: #2d6da3;">MFA</a>
            <a href="<?php echo base_url('view/compare')?>" class="navrbd">Comparing</a>
            <a href="<?php echo base_url('view/predict_form')?>" class="nav">Estimation</a>
            <a href="<?php echo base_url('view/plan_form') ?>" class="nav">Planning</a>
            <?php if(isset($_SESSION['username'])) { ?>
                <a href="<?php echo base_url('view/favor') ?>" class="nav">Favourite</a>
                <a href="<?php echo base_url('view/port') ?>" class="nav">Portfolio</a>
            <?php } ?>
            <?php if (isset($_SESSION['username'])) { ?>
                <div class="navsignout">
                    <span style="margin-right: 10px;"><?php echo $_SESSION['username'] ?></span>
                    <a href="<?php echo base_url('logout') ?>" style="color: #2d6da3; cursor: pointer; text-decoration: none;">Sign Out</a>
                </div>
            <?php } else { ?>
                <div class="navsignin" onclick="document.getElementById('signin').style.display='block'" style="width:auto; color: #2d6da3;">Sign In</div>
            <?php } ?>
        </div>
    </div>

    <div id="signin" class="modal">
  
        <form id="signinForm" class="modal-content animate" action="<?php echo base_url('login') ?>" method="POST">
            <div class="logocontainer">
                <a href="<?php echo base_url('view/index') ?>" style="text-decoration: none; color: #2d6da3;"><h1>MFA</h1></a>
                <span onclick="document.getElementById('signin').style.display='none'" class="close" title="Close">&times;</span>
            </div>
      
            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" id="uname_in" required>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_in" required>

                <div class="error_text"><span></span></div>
              
                <button type="submit">Login</button>
            </div>
      
            <div class="container" style="background-color:#f1f1f1">
                <span class="signup" onclick="document.getElementById('signup').style.display='block' ,
                document.getElementById('signin').style.display='none'" style="width:auto;">Sign Up</span>
            </div>
        </form>
    </div>

    <div id="signup" class="modal">

        <form id="signupForm" class="modal-content animate" action="<?php echo base_url('register') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Sign Up</h1>
                <span onclick="document.getElementById('signup').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <div class="error_text_uname"><span></span></div>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_up" required>
                <div class="error_text_psw"><span></span></div>

                <label for="psw"><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Confirm Password" name="psw1" required>
                <div class="error_text_psw1"><span></span></div>
                
                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email" required>
                <div class="error_text_email"><span></span></div>

                <button type="submit">Create Account</button>
            </div>
        </form>
            
    </div>

    <div id="addfund" class="modal">

        <form id="addForm" class="modal-content animate" action="<?php echo base_url('port_add') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Add to Portfolio</h1>
                <span onclick="document.getElementById('addfund').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="fund_code"><b>Fund Code</b></label>
                <input type="text" placeholder="Enter Fund Code" name="fund_code" id="fund_code" value="" readonly>

                <label for="owned_units"><b>Owned Units</b></label>
                <input type="text" placeholder="Enter Owned Units" name="amount" required>

                <label for="buy_price"><b>Buy Price</b></label>
                <input type="text" placeholder="Enter Buy Price" name="buy_price" required>

                <button type="submit">Add</button>
            </div>
        </form>
        
    </div>

    <div id="addfavfund" class="modal">

        <form id="addfavForm" class="modal-content animate" action="<?php echo base_url('fav_add') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Add Favourite Fund</h1>
                <span onclick="document.getElementById('addfavfund').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="fund_name"><b>Fund Code</b></label>
                <select name="fund_name" id="fund_select" required>
                    <?php foreach($name as $n=>$val){ ?>
                                                <optgroup label="<?php echo $n; ?>">
                                                    <?php foreach($val as $v=>$tval){ ?>
                                                    <optgroup label="<?php echo $v; ?>">
                                                        <?php foreach($tval as $t){ ?>
                                                            <option value="<?php echo $t->fund_symbol; ?>"><?php echo $t->fund_symbol." - ".$t->fund_name; ?></option>
                                                        <?php } ?>
                                                    </optgroup>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                </select>

                <button type="submit">Add</button>
            </div>
        </form>
            
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">
                        
                        <div class="header" style="margin-bottom: 20px; margin-top: 20px;">
                            <h1>My Favourite Fund</h1>
                        </div>

                        <div class="addbutton" style="width: 25%;">
                            <button onclick="document.getElementById('addfavfund').style.display='block'" style="width:auto; margin-bottom: 20px;">Add Mutual Fund</button>
                        </div>

                        <table id="example" class="ui celled table" style="width:100%;"> 
                            <thead> 
                                <tr> 
                                    <th>Symbol</th>
                                    <th>Current NAV</th> 
                                    <th>NAV Change (฿)</th> 
                                    <th>NAV Change (%)</th> 
                                    <th>Updated at</th>
                                    <th></th>
                                </tr> 
                            </thead> 
                            <tbody> 
                            <?php 
                                    foreach($data as $row){
                                ?> 
                                <tr>
                                    <td>
                                        <a href="<?php echo base_url('view/detail/'.$row['fund_id']) ?>" style="color: #2d6da3; text-decoration: none;">
                                            <?php echo $row['fund_name']; ?>
                                        </a>
                                    </td>
                                    <td><?php echo $row['nav']; ?> ฿</td>
                                    <td><?php echo $row['change']; ?></td>
                                    <td><?php echo $row['%change']; ?>%</td>
                                    <td><?php echo $row['date']; ?></td>
                                    <td>
                                        <div class="editorcon">
                                            <span class="add_sec" onclick="document.getElementById('addfund').style.display='block',
                                                document.getElementById('fund_code').value='<?php echo $row['fund_name']; ?>'"
                                                style="cursor: pointer;">
                                                    <i class="fas fa-file-import"></i>
                                            </span>
                                            <span class="del_sec" style="cursor: pointer;" >
                                                <form action="<?php echo base_url('fav_del') ?>" method="POST" name="del_<?php echo $row['fund_name']; ?>">
                                                    <input type="hidden" name="fund_name" value="<?php echo $row['fund_name']; ?>">
                                                    <i class="far fa-trash-alt" onclick="document.forms['del_<?php echo $row['fund_name']; ?>'].submit()"></i>
                                                </form>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                    <?php } ?>
                            </tbody>
                            <tfoot> 
                                <tr> 
                                    <th>Symbol</th> 
                                    <th>Current NAV</th> 
                                    <th>NAV Change (฿)</th> 
                                    <th>NAV Change (%)</th> 
                                    <th>Updated At</th>
                                    <th></th>
                                </tr> 
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

    <script>
        //DataTable
        $(document).ready(function() {
            $('#example').DataTable( {
                
                "paging":   false,
                "info":     false,
                "searching": false,

                "columns": [
                    null,null,null,null,null,null,{ "orderable": false }
                ]
                
            } );
        } );
        
        $("#fund_select").selectize({
            sortField: 'text'
        });
        
        $("#addForm").validate({

            rules:{
                amount:{
                    required:true,
                    number:true,
                    min:1
                },
                buy_price:{
                    required:true,
                    number:true,
                    min:1
                }
            }

        });

        
        $("#signinForm").validate({   

            onkeyup: false,
            onclick: false,
            onfocusout: false,

            rules:{
                psw:{
                    remote:{
                        type: 'post',
                        url: 'login_validation',
                        data: {
                            uname: function(){
                                return $("#uname_in").val();
                            }
                        },
                        dataType: 'json'
                    }
                }
            },
            messages:{
                psw:"Incorrect Username or Password"
            },
            errorPlacement: function(error, element) {
                error.appendTo(".error_text span");
            }
        });

        $("#signupForm").validate({
            rules:{
                uname:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12],
                    remote:{
                        type:'post',
                        url:'uname_available'
                    }
                },
                psw:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12]
                },
                psw1:{
                    equalTo: "#psw_up"
                },
                email:{
                    required:true,
                    email:true,
                    remote:{
                        type:'post',
                        url:'email_available'
                    }
                }
            },
            messages:{
                uname:"Username already taken.",
                email:"Email already in used."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "uname" )
                    error.appendTo(".error_text_uname span");
                else if  (element.attr("name") == "psw" )
                    error.appendTo(".error_text_psw span");
                else if (element.attr("name") == "psw1" )
                    error.appendTo(".error_text_psw1 span");
                else 
                    error.appendTo(".error_text_email span");
            }
        });
    </script>

</body>
</html>