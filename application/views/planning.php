<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds Assistant</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">

    <script src="<?php echo base_url('assets/js/script.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

    <script>
    
        function formValidation(){
            
            var init = parseInt(document.forms['planForm']['initial'].value);
            var tar = parseInt(document.forms['planForm']['target'].value);

            console.log(tar<init);

            if(tar < init){
                alert('Your target amount is less than initial.');
                return false;
            }

            return true;
        }
    
    
    </script>

</head>
<body>
    
    <div class="navbar">
        <div class="navcon">
            <a href="<?php echo base_url('view/index') ?>" class="logo" style="color: #2d6da3;">MFA</a>
            <a href="<?php echo base_url('view/compare')?>" class="navrbd">Comparing</a>
            <a href="<?php echo base_url('view/predict_form')?>" class="nav">Estimation</a>
            <a href="<?php echo base_url('view/plan_form') ?>" class="nav">Planning</a>
            <?php if(isset($_SESSION['username'])) { ?>
                <a href="<?php echo base_url('view/favor') ?>" class="nav">Favourite</a>
                <a href="<?php echo base_url('view/port') ?>" class="nav">Portfolio</a>
            <?php } ?>
            <?php if (isset($_SESSION['username'])) { ?>
                <div class="navsignout">
                    <span style="margin-right: 10px;"><?php echo $_SESSION['username'] ?></span>
                    <a href="<?php echo base_url('logout') ?>" style="color: #2d6da3; cursor: pointer; text-decoration: none;">Sign Out</a>
                </div>
            <?php } else { ?>
                <div class="navsignin" onclick="document.getElementById('signin').style.display='block'" style="width:auto; color: #2d6da3;">Sign In</div>
            <?php } ?>
        </div>
    </div>

    <div id="signin" class="modal">
  
        <form id="signinForm" class="modal-content animate" action="<?php echo base_url('login') ?>" method="POST">
            <div class="logocontainer">
                <a href="<?php echo base_url('view/index') ?>" style="text-decoration: none; color: #2d6da3;"><h1>MFA</h1></a>
                <span onclick="document.getElementById('signin').style.display='none'" class="close" title="Close">&times;</span>
            </div>
      
            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" id="uname_in" required>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_in" required>

                <div class="error_text"><span></span></div>
              
                <button type="submit">Login</button>
            </div>
      
            <div class="container" style="background-color:#f1f1f1">
                <span class="signup" onclick="document.getElementById('signup').style.display='block' ,
                document.getElementById('signin').style.display='none'" style="width:auto;">Sign Up</span>
            </div>
        </form>
    </div>

    <div id="signup" class="modal">

        <form id="signupForm" class="modal-content animate" action="<?php echo base_url('register') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Sign Up</h1>
                <span onclick="document.getElementById('signup').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <div class="error_text_uname"><span></span></div>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_up" required>
                <div class="error_text_psw"><span></span></div>

                <label for="psw"><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Confirm Password" name="psw1" required>
                <div class="error_text_psw1"><span></span></div>
                
                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email" required>
                <div class="error_text_email"><span></span></div>

                <button type="submit">Create Account</button>
            </div>
        </form>
            
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">
                        
                        <div class="header" style="margin-bottom: 20px; margin-top: 20px;">
                            <h1>Mutual Fund Planning</h1>
                        </div>
                        <form name ="plan" class="plan" id="planForm" onsubmit="return formValidation()" action="<?php echo base_url('view/plan_result') ?>" method="POST">
                            <div class="container">

                                <label for="initial"><b>Initial Investment</b></label>
                                <input type="text" value="" placeholder="Enter Initial Investment" id="initial" name="initial" required><br>
                                
                                <label for="target"><b>Target Investment</b></label>
                                <input type="text" value="" placeholder="Enter Target Investment" id="target" name="target" required><br>

                                <label for="year"><b>Investment Year</b></label>
                                <input type="text" value="" placeholder="Enter Investment Year (0 ~ 40 years)" name="year" required><br>

                                <label for="risk"><b>Risk</b></label>
                                <select name="risk">
                                    <option value="1">Low Risk</option>
                                    <option value="2">Medium Risk</option>
                                    <option value="3">High Risk</option>
                                </select>

                                <div class="fund_firm_head" style="margin-bottom: 8px;">
                                    <label for="fund_firm[]"><b>Fund Investment Firms that you would invest</b></label>
                                </div>
                                <table width="100%">
                                    <tr>
                                        <br>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="KTAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/ktam.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="KrungsriAsset" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/krungsriasset.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="KAsset" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/kasset.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="CIMBPrincipal" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/cimbprincipal.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="SolarisFund" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/solarisfund.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="TMBAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/tmbam.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="TALISAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/talisfund.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="TISCOAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/tiscoam.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="SCBAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/scbam.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="TFUND" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/tfund.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="BCAP" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/bcap.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="PhatraAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/phatra.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="UOBAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/uobam.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="LHFUND" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/lhfund.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="ONEAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/oneam.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="SKFM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/skfm.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="ABERDEEN" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/aberdeen.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="MFC" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/mfc.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="ASSETFUND" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/assetfund.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="BBLAM" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/bblam.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="checkcon">
                                                <input type="checkbox" value="PAMC" name="fund_firm[]">
                                                <span class="checkmark"></span>
                                                <div class="firmpic">
                                                    <img src="<?php echo base_url('assets/fund_logo/pamc.gif');?>">
                                                </div>
                                            </label>
                                        </td>
                                    </tr>
                                </table>        
                                
                                <div class="error_form"><span></span></div>
                                <button type="submit" style="width: 30%;">Next</button>
                            </div>
                        </form>

                        <script>
                        
                            $("#planForm").validate({
                                rules:{
                                    initial:{
                                        required: true,
                                        number: true,
                                        min: 1
                                    },
                                    target:{
                                        required: true,
                                        number: true

                                    },
                                    year:{
                                        required: true,
                                        number: true,
                                        min:1,
                                        max:40
                                    },
                                    'fund_firm[]':{
                                        required: true,
                                        minlength: 1
                                    }
                                },
                                messages:{
                                    initial:'Please fill out your initial invesment',
                                    target:'Please fill out your target investment',
                                    year:'Please specify year',
                                    'fund_firm[]': 'Please select at least one firm'
                                },
                                errorPlacement: function(error, element) {
                                    error.appendTo(".error_form span");
                                }
                            });

                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

    <script>
    
    
        $("#signinForm").validate({   

            onkeyup: false,
            onclick: false,
            onfocusout: false,

            rules:{
                psw:{
                    remote:{
                        type: 'post',
                        url: 'login_validation',
                        data: {
                            uname: function(){
                                return $("#uname_in").val();
                            }
                        },
                        dataType: 'json'
                    }
                }
            },
            messages:{
                psw:"Incorrect Username or Password"
            },
            errorPlacement: function(error, element) {
                error.appendTo(".error_text span");
            }
        });

        $("#signupForm").validate({
            rules:{
                uname:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12],
                    remote:{
                        type:'post',
                        url:'uname_available'
                    }
                },
                psw:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12]
                },
                psw1:{
                    equalTo: "#psw_up"
                },
                email:{
                    required:true,
                    email:true,
                    remote:{
                        type:'post',
                        url:'email_available'
                    }
                }
            },
            messages:{
                uname:"Username already taken.",
                email:"Email already in used."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "uname" )
                    error.appendTo(".error_text_uname span");
                else if  (element.attr("name") == "psw" )
                    error.appendTo(".error_text_psw span");
                else if (element.attr("name") == "psw1" )
                    error.appendTo(".error_text_psw1 span");
                else 
                    error.appendTo(".error_text_email span");
            }
        });
    
    </script>

</body>
</html>