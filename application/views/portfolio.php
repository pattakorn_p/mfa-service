<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds Assistant</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">

    <script src="<?php echo base_url('assets/js/script.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.default.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>
<body>
    
<div class="navbar">
        <div class="navcon">
            <a href="<?php echo base_url('view/index') ?>" class="logo" style="color: #2d6da3;">MFA</a>
            <a href="<?php echo base_url('view/compare')?>" class="navrbd">Comparing</a>
            <a href="<?php echo base_url('view/predict_form')?>" class="nav">Estimation</a>
            <a href="<?php echo base_url('view/plan_form') ?>" class="nav">Planning</a>
            <?php if(isset($_SESSION['username'])) { ?>
                <a href="<?php echo base_url('view/favor') ?>" class="nav">Favourite</a>
                <a href="<?php echo base_url('view/port') ?>" class="nav">Portfolio</a>
            <?php } ?>
            <?php if (isset($_SESSION['username'])) { ?>
                <div class="navsignout">
                    <span style="margin-right: 10px;"><?php echo $_SESSION['username'] ?></span>
                    <a href="<?php echo base_url('logout') ?>" style="color: #2d6da3; cursor: pointer; text-decoration: none;">Sign Out</a>
                </div>
            <?php } else { ?>
                <div class="navsignin" onclick="document.getElementById('signin').style.display='block'" style="width:auto; color: #2d6da3;">Sign In</div>
            <?php } ?>
        </div>
    </div>

    <div id="signin" class="modal">
  
        <form id="signinForm" class="modal-content animate" action="<?php echo base_url('login') ?>" method="POST">
            <div class="logocontainer">
                <a href="<?php echo base_url('view/index') ?>" style="text-decoration: none; color: #2d6da3;"><h1>MFA</h1></a>
                <span onclick="document.getElementById('signin').style.display='none'" class="close" title="Close">&times;</span>
            </div>
      
            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" id="uname_in" required>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_in" required>

                <div class="error_text"><span></span></div>
              
                <button type="submit">Login</button>
            </div>
      
            <div class="container" style="background-color:#f1f1f1">
                <span class="signup" onclick="document.getElementById('signup').style.display='block' ,
                document.getElementById('signin').style.display='none'" style="width:auto;">Sign Up</span>
            </div>
        </form>
    </div>

    <div id="signup" class="modal">

        <form id="signupForm" class="modal-content animate" action="<?php echo base_url('register') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Sign Up</h1>
                <span onclick="document.getElementById('signup').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <div class="error_text_uname"><span></span></div>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_up" required>
                <div class="error_text_psw"><span></span></div>

                <label for="psw"><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Confirm Password" name="psw1" required>
                <div class="error_text_psw1"><span></span></div>
                
                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email" required>
                <div class="error_text_email"><span></span></div>

                <button type="submit">Create Account</button>
            </div>
        </form>
            
    </div>

    <div id="addfund" class="modal">

        <form id="addForm" class="modal-content animate" action="<?php echo base_url('port_add') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Add Fund</h1>
                <span onclick="document.getElementById('addfund').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="fund_code"><b>Fund Code</b></label>
                <select name="fund_code" id="fund_select" required>
                    <?php foreach($name as $n=>$val){ ?>
                                                <optgroup label="<?php echo $n; ?>">
                                                    <?php foreach($val as $v=>$tval){ ?>
                                                    <optgroup label="<?php echo $v; ?>">
                                                        <?php foreach($tval as $t){ ?>
                                                            <option value="<?php echo $t->fund_symbol; ?>"><?php echo $t->fund_symbol." - ".$t->fund_name; ?></option>
                                                        <?php } ?>
                                                    </optgroup>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                </select>

                <label for="owned_units"><b>Owned Units</b></label>
                <input type="text" placeholder="Enter Owned Units" name="amount" required>

                <label for="buy_price"><b>Buy Price</b></label>
                <input type="text" placeholder="Enter Buy Price" name="buy_price" required>

                <button type="submit">Add</button>
            </div>
        </form>
            
    </div>

    <div id="editfund" class="modal">

        <form id="editForm" class="modal-content animate" action="<?php echo base_url('port_edit') ?>" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Edit Fund</h1>
                <span onclick="document.getElementById('editfund').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="fund_code"><b>Fund Code</b></label>
                <input type="text" placeholder="Enter Fund Code" name="fund_code" id="fund_code" value="" readonly  >

                <label for="owned_units"><b>Owned Units</b></label>
                <input type="text" placeholder="Enter Owned Units" name="amount" id="amount" value="" required>

                <label for="buy_price"><b>Buy Price</b></label>
                <input type="text" placeholder="Enter Buy Price" name="buy_price" id="buy_price" value="" required>

                <button type="submit">Edit</button>
            </div>
        </form>
            
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">
                        
                    <div class="portheader" style="margin-bottom: 20px; margin-top: 20px;">
                            <h1>My Portfolio</h1>
                        </div>

                        <div class="addbutton" style="width: 25%;">
                            <button onclick="document.getElementById('addfund').style.display='block'" style="width:auto;">Add Mutual Fund</button>
                        </div>

                        <table id="example" class="ui celled table" style="width:100%;"> 
                            <thead> 
                                <tr> 
                                    <th>Symbol</th> 
                                    <th>Volume</th> 
                                    <th>Average Cost</th> 
                                    <th>Market Price</th> 
                                    <th>Total Value</th> 
                                    <th>Market Value</th> 
                                    <th>Profit/Loss</th> 
                                    <th>%Profit/Loss</th>
                                    <th></th>
                                </tr> 
                            </thead> 
                            <tbody> 
                                <?php 
                                    foreach($data as $row){
                                ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo base_url('view/detail/'.$row['fund_id']) ?>" style="color: #2d6da3; text-decoration: none;">
                                            <?php echo $row['fund_name']; ?>
                                        </a>
                                    </td> 
                                    <td><?php echo $row['amount']; ?></td> 
                                    <td><?php echo $row['average_cost']." ฿"; ?></td> 
                                    <td><?php echo $row['market_price']." ฿"; ?></td> 
                                    <td><?php echo $row['sum_price']." ฿"; ?></td> 
                                    <td><?php echo $row['sum_market_price']." ฿"; ?></td>
                                    <?php if ( $row['profit-loss'] < 0) { ?>
                                        <td style="color: red"><?php echo $row['profit-loss']." ฿"; ?></td>
                                    <?php } else { ?>
                                        <td style="color: green"><?php echo $row['profit-loss']." ฿"; ?></td> 
                                    <?php } ?>
                                    <?php if ( $row['pl-percentage'] < 0) { ?>
                                        <td style="color: red"><?php echo $row['pl-percentage']." %"; ?></td>
                                    <?php } else { ?>
                                        <td style="color: green"><?php echo $row['pl-percentage']." %"; ?></td> 
                                    <?php } ?>
                                    <td>
                                        <div class="editorcon">
                                            <span class="edit_sec" onclick="document.getElementById('editfund').style.display='block', 
                                                document.getElementById('fund_code').value='<?php echo $row['fund_name']; ?>',
                                                document.getElementById('amount').value='<?php echo $row['amount']; ?>',
                                                document.getElementById('buy_price').value='<?php echo $row['average_cost']; ?>'"
                                                style="cursor: pointer;">
                                                    <i class="far fa-edit"></i>
                                            </span>
                                            <span class="del_sec" style="cursor: pointer;" >
                                                <form action="<?php echo base_url('port_del') ?>" method="POST" name="del_<?php echo $row['fund_name']; ?>">
                                                    <input type="hidden" name="fund_name" value="<?php echo $row['fund_name']; ?>">
                                                    <i class="far fa-trash-alt" onclick="document.forms['del_<?php echo $row['fund_name']; ?>'].submit()"></i>
                                                </form>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                    <?php } ?>
                            </tbody> 
                            <tfoot> 
                                <tr> 
                                    <th>Total</th> 
                                    <th></th> 
                                    <th></th> 
                                    <th></th> 
                                    <th></th> 
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr> 
                            </tfoot> 
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

    <script>
        
        $("#fund_select").selectize({
                    sortField: 'text'
        });
        //DataTable
        $(document).ready(function() {
            $('#example').DataTable( {
                
                "paging":   false,
                "info":     false,
                "searching": false,

                "columns": [
                    null,null,null,null,null,null,null,null,{ "orderable": false }
                ],
                
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
        
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\฿,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var intPer = function ( j ) {
                        return typeof j === 'string' ?
                            j.replace(/[\%]/g, '')*1 :
                            typeof j === 'number' ?
                                j : 0;
                    };
        
                    // Total over all pages
                    total_owned = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    total_marketcap = api
                        .column( 5 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    
                    total_profitloss = api
                        .column( 6 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        total_owned.toFixed(2) + ' ฿'
                    );

                    $( api.column( 5 ).footer() ).html(
                        total_marketcap.toFixed(2) + ' ฿'
                    );

                    if (total_profitloss < 0) {
                        $( api.column( 6 ).footer() ).html(
                            '<p style="color: red">'+ total_profitloss.toFixed(2) + ' ฿</p>'
                        );
                    } else {
                        $( api.column( 6 ).footer() ).html(
                            '<p style="color: green">'+ total_profitloss.toFixed(2) + ' ฿</p>'
                        );
                    }
                    
                    var total_plpercentage = ((total_profitloss / total_owned) * 100).toFixed(2);
                    if (total_plpercentage < 0) {
                        $( api.column( 7 ).footer() ).html(
                            '<p style="color: red">'+ total_plpercentage + ' %</p>'
                        );
                    } else {
                        $( api.column( 7 ).footer() ).html(
                            '<p style="color: green">'+ total_plpercentage + ' %</p>'
                        );
                    }

                }
            } );
        } );

        $("#addForm").validate({

            rules:{
                amount:{
                    required:true,
                    number:true,
                    min:1
                },
                buy_price:{
                    required:true,
                    number:true,
                    min:1
                }
            }

        });

        $("#editForm").validate({

            rules:{
                amount:{
                    required:true,
                    number:true,
                    min:1
                },
                buy_price:{
                    required:true,
                    number:true,
                    min:1
                }
            }

        });

        
        $("#signinForm").validate({   

            onkeyup: false,
            onclick: false,
            onfocusout: false,

            rules:{
                psw:{
                    remote:{
                        type: 'post',
                        url: 'login_validation',
                        data: {
                            uname: function(){
                                return $("#uname_in").val();
                            }
                        },
                        dataType: 'json'
                    }
                }
            },
            messages:{
                psw:"Incorrect Username or Password"
            },
            errorPlacement: function(error, element) {
                error.appendTo(".error_text span");
            }
        });

        $("#signupForm").validate({
            rules:{
                uname:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12],
                    remote:{
                        type:'post',
                        url:'uname_available'
                    }
                },
                psw:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12]
                },
                psw1:{
                    equalTo: "#psw_up"
                },
                email:{
                    required:true,
                    email:true,
                    remote:{
                        type:'post',
                        url:'email_available'
                    }
                }
            },
            messages:{
                uname:"Username already taken.",
                email:"Email already in used."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "uname" )
                    error.appendTo(".error_text_uname span");
                else if  (element.attr("name") == "psw" )
                    error.appendTo(".error_text_psw span");
                else if (element.attr("name") == "psw1" )
                    error.appendTo(".error_text_psw1 span");
                else 
                    error.appendTo(".error_text_email span");
            }
        });
    </script>

</body>
</html>