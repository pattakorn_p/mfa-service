<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds</title>
    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
</head>
<body>  
    
    <div class="navbar">
        <div class="navcon">
            <a href="#" class="navrbd">nav1</a> 
            <a href="#">nav2</a> 
            <a href="#">nav3</a>
            <a href="#">nav4</a> 
            <a href="#">nav5</a> 
            <a href="#">nav6</a> 
            <a href="#">nav7</a>  
        </div>
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            <div class="logocon">
                <a href="<?php echo base_url('view/index') ?>"><h1>logo</h1></a>
            </div>
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">
                        
                        <a href="<?php echo base_url('view/detail/TMB50') ?>">fund info</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

</body>
</html>