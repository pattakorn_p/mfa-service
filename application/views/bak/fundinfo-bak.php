<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds Info</title>
    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
</head>
<body>
    <div class="navbar">
        <div class="navcon">
            <a href="#" class="navrbd">nav1</a> 
            <a href="#">nav2</a> 
            <a href="#">nav3</a>
            <a href="#">nav4</a> 
            <a href="#">nav5</a> 
            <a href="#">nav6</a> 
            <a href="#">nav7</a>  
        </div>
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            <div class="logocon">
                <a href="<?php echo base_url('view/index') ?>"><h1>logo</h1></a>
            </div>
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">

                        <div class="fund_header">
                            <div class="fund_logo">
                                FUND LOGO
                            </div>
                            <div class="fund_title">
                                <div class="fund_code">
                                    <?php echo $fund_symbol; ?>
                                </div>
                                <div class="fund_name">
                                    <?php echo $fund_name; ?>
                                </div>
                            </div>
                            <div class="fund_risk">
                                RISK : ...
                            </div>
                        </div>

                        <div class="fund_body">
                            <div class="fund_navcon">
                                <div class="fund_navhead">Net Asset Value</div>
                                <div class="fund_navbody"><?php echo $nav ?> baht</div>
                            </div>
                            <div class="fund_navchangecon">
                                <div class="fund_navhead">NAV Change</div>
                                <div class="fund_navcbath"><?php echo $change; ?></div>
                                <div class="fund_navcperc">+/- 00.00 %</div>
                            </div>
                            <div class="fund_navupdatecon">
                                update at <?php echo date('d/m/Y'); ?>
                            </div>
                        </div>
                        
                        <div class="fund_graphcon">FUND GRAPH</div>

                        <div class="fund_detailcon">
                            <div class="fund_detailleft">
                                <p>Fund Type : <?php echo $fund_type; ?></p>
                                <p>Fund Detail :</p>
                                <p>...</p>
                            </div>
                            <div class="fund_detailright">
                                <p>Bid Price : <?php echo $bid; ?></p>
                                <p>Offer Price : <?php echo $offer; ?></p>
                                <p>Total Investment Units : ...</p>
                                <p>Total Property Value : <?php echo $assetvalue; ?></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

</body>
</html>