<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutual Funds Assistant</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">

    <script src="<?php echo base_url('assets/js/script.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/jquery-form/form@4.2.2/dist/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</head>
<body>

    <div class="navbar">
        <div class="navcon">
            <a href="<?php echo base_url('view/index') ?>" class="logo" style="color: #2d6da3;">MFA</a>
            <a href="<?php echo base_url('view/compare')?>" class="navrbd">Comparing</a>
            <a href="<?php echo base_url('view/predict_form')?>" class="nav">Estimation</a>
            <a href="<?php echo base_url('view/plan_form') ?>" class="nav">Planning</a>
            <?php if(isset($_SESSION['username'])) { ?>
                <a href="<?php echo base_url('view/favor') ?>" class="nav">Favourite</a>
                <a href="<?php echo base_url('view/port') ?>" class="nav">Portfolio</a>
            <?php } ?>
            <?php if (isset($_SESSION['username'])) { ?>
                <div class="navsignout">
                    <span style="margin-right: 10px;"><?php echo $_SESSION['username'] ?></span>
                    <a href="<?php echo base_url('logout') ?>" style="color: #2d6da3; cursor: pointer; text-decoration: none;">Sign Out</a>
                </div>
            <?php } else { ?>
                <div class="navsignin" onclick="document.getElementById('signin').style.display='block'" style="width:auto; color: #2d6da3;">Sign In</div>
            <?php } ?>
        </div>
    </div>

    <div id="signin" class="modal">
  
        <form id="signinForm" class="modal-content animate" action="<?php echo base_url('login') ?>" method="POST">
            <div class="logocontainer">
                <a href="<?php echo base_url('view/index') ?>" style="text-decoration: none; color: #2d6da3;"><h1>MFA</h1></a>
                <span onclick="document.getElementById('signin').style.display='none'" class="close" title="Close">&times;</span>
            </div>
      
            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" id="uname_in" required>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_in" required>

                <div class="error_text"><span></span></div>
              
                <button type="submit">Login</button>
            </div>
      
            <div class="container" style="background-color:#f1f1f1">
                <span class="signup" onclick="document.getElementById('signup').style.display='block' ,
                document.getElementById('signin').style.display='none'" style="width:auto;">Sign Up</span>
            </div>
        </form>
    </div>

    <div id="signup" class="modal">

        <form id="signupForm" class="modal-content animate" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Sign Up</h1>
                <span onclick="document.getElementById('signup').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <div class="error_text_uname"><span></span></div>
      
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id="psw_up" required>
                <div class="error_text_psw"><span></span></div>

                <label for="psw"><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Confirm Password" name="psw1" required>
                <div class="error_text_psw1"><span></span></div>
                
                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email" required>
                <div class="error_text_email"><span></span></div>

                <button type="submit">Create Account</button>
            </div>
        </form>
            
    </div>

    <div id="addfund" class="modal">

        <form id="addForm" class="modal-content animate" onsubmit="ajaxPortAdd()" method="POST">
            <div class="logocontainer">
                <h1 style="color: #2d6da3;">Add to Portfolio</h1>
                <span onclick="document.getElementById('addfund').style.display='none'" class="close" title="Close">&times;</span>
            </div>

            <div class="container">
                <label for="fund_code"><b>Fund Code</b></label>
                <input type="text" placeholder="Enter Fund Code" name="fund_code" id="fund_code" readonly>

                <label for="owned_units"><b>Owned Units</b></label>
                <input type="text" placeholder="Enter Owned Units" name="amount" required>

                <label for="buy_price"><b>Buy Price</b></label>
                <input type="text" placeholder="Enter Buy Price" name="buy_price" required>

                <button type="submit">Add</button>
            </div>
        </form>
        
    </div>

    <div class="bgpadding">
        <div class="widthcontrol">
            <div class="colcon">
                <div class="contentcol">
                    <div class="index">

                        <div class="fund_header">
                            <div class="fund_logo">
                                <img src = "<?php echo base_url('assets/fund_logo/'.$fund_am_symbol.'.gif');?>">
                            </div>
                            <div class="fund_title">
                                <div class="fund_code">
                                    <?php echo $fund_symbol; ?>
                                </div>
                                <div class="fund_name">
                                    <?php echo $fund_name; ?>
                                </div>
                            </div>
                            <div class="fund_more">
                                <div class="fund_risk">
                                    RISK : <?php echo $fund_risk; ?>
                                </div>
                                <div class="buttoncon">
                                    <?php if(isset($_SESSION['username'])) { ?>
                                    <div class="fund_button" style="margin-right: 20px;">
                                        <form onclick="ajaxFavAdd(this)" method="POST" id="favAdd" name="add_fav_fund">
                                            <input type="hidden" name="fund_name" value="<?php echo $fund_symbol; ?>">
                                            <button type="submit">Add to Favourite</button>
                                        </form>
                                    </div>
                                    <div class="fund_button">
                                        <button onclick="document.getElementById('addfund').style.display='block',
                                        document.getElementById('fund_code').value='<?php echo $fund_symbol; ?>'" style="width:auto;">Add to Portfolio</button>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="fund_body">
                            <div class="fund_navcon">
                                <div class="fund_navhead">Current NAV</div>
                                <div class="fund_navbody"><?php echo $nav ?> ฿</div>
                            </div>
                            <div class="fund_navchangecon">
                                <div class="fund_navhead">NAV Change</div>
                                <?php if ($change < 0) {?>
                                    <div class="fund_navcbath" style="color: red;"><?php echo $change; ?> ฿</div>
                                    <div class="fund_navcperc" style="color: red;"><?php echo number_format(($change / $nav) * 100, 4, '.', '') ?> %</div>
                                <?php }else if ($change > 0){ ?>
                                    <div class="fund_navcbath" style="color: green;"><?php echo $change; ?> ฿</div>
                                    <div class="fund_navcperc" style="color: green;"><?php echo number_format(($change / $nav) * 100, 4, '.', '') ?> %</div>
                                <?php } else { ?>
                                    <div class="fund_navcbath" style="color: green;"><?php echo $change;?> ฿</div>
                                    <div class="fund_navcperc" style="color: green;">0%</div>
                                <?php } ?>
                            </div>
                            <div class="fund_navupdatecon">
                                update at <?php echo $date; ?>
                            </div>
                        </div>
                        
                        <div class="fund_graphcon">
                            <canvas id="chart"></canvas>
                        </div>

                        <div class="fund_detailcon">
                            <div class="fund_detailleft">
                                <p>Fund Type : <?php echo $fund_type; ?></p></p>
                                <p>Total Property Value : <?php echo $assetvalue; ?> ฿</p>
                            </div>
                            <div class="fund_detailright">
                                <p>Bid Price : <?php echo $bid; ?> ฿</p>
                                <p>Offer Price : <?php echo $offer; ?> ฿</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="widthcontrol_footer">
            <p>Copyright &copy; 2018 | Project</p>
        </div>
    </footer>

    <script>

        var base_url = "<?php echo base_url() ?>";
        var h_price = <?php echo json_encode($historic_price); ?>;
        var h_date = <?php echo json_encode($historic_date); ?>;
        var ctx = document.getElementById('chart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',

            data: {
                datasets: [{
                    fill: false,
                    label: 'NAV Change (฿)',
                    borderColor: '#2d6da3',
                    backgroundColor: '#2d6da3',
                    //x-axis navchange
                    data: h_price
                }],
                //y-axis date (month=0-11) format='Month Date Year' ex:Oct 10 2018
                labels: h_date
            },
            options: {
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true
                        },
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: false,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'NAV Change (฿)'
                        }
                    }]
                }
            }
        });

        $("#signinForm").validate({   

            onkeyup: false,
            onclick: false,
            onfocusout: false,

            rules:{
                psw:{
                    remote:{
                        type: 'post',
                        url: '../login_validation',
                        data: {
                            uname: function(){
                                return $("#uname_in").val();
                            }
                        },
                        dataType: 'json'
                    }
                }
            },
            messages:{
                psw:"Incorrect Username or Password"
            },
            errorPlacement: function(error, element) {
                error.appendTo(".error_text span");
            }
        });

        $("#signupForm").validate({
            rules:{
                uname:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12],
                    remote:{
                        type:'post',
                        url:'../uname_available'
                    }
                },
                psw:{
                    required:true,
                    alphanumeric:true,
                    rangelength:[4, 12]
                },
                psw1:{
                    equalTo: "#psw_up"
                },
                email:{
                    required:true,
                    email:true,
                    remote:{
                        type:'post',
                        url:'../email_available'
                    }
                }
            },
            messages:{
                uname:"Username already taken.",
                email:"Email already in used."
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "uname" )
                    error.appendTo(".error_text_uname span");
                else if  (element.attr("name") == "psw" )
                    error.appendTo(".error_text_psw span");
                else if (element.attr("name") == "psw1" )
                    error.appendTo(".error_text_psw1 span");
                else 
                    error.appendTo(".error_text_email span");
            }
        });

        $("#addForm").validate({

            rules:{
                amount:{
                    required:true,
                    number:true,
                    min:1
                },
                buy_price:{
                    required:true,
                    number:true,
                    min:1
                }
            }

        });

        function ajaxPortAdd(){
            $('#addForm').ajaxSubmit({url:base_url+'port_add', type:'post'})
        }
        function ajaxFavAdd(el){
            alert('Added');
            $(el).ajaxSubmit({url:base_url+'fav_add', type:'post'});
        }
        </script>

</body>
</html>