<?php

    Class Fetcher extends CI_Model{

        public function __construct(){
            $this->load->model('Curl');
        }

        public function fetchByAM($am){

            $list = array();

            /*$this->db->where('fund_am_symbol', $am);
            $this->db->select('fund_symbol, fund_name, fund_type');
            $query = $this->db->get('funds');

            $fund_list = $query->result();*/

            $this->db->select('fund_type');
            $this->db->group_by('fund_type');
            $this->db->where('fund_am_symbol', $am);
            $query = $this->db->get('funds');
            
            $type_list = $query->result();

            foreach($type_list as $t){
                
                $this->db->where('fund_am_symbol', $am);
                $this->db->where('fund_type', $t->fund_type);
                $this->db->select('fund_id, fund_symbol, fund_name, fund_type');
                $query2 = $this->db->get('funds');

                $fund_list = $query2->result();
                $list[$t->fund_type] = $fund_list; 
            }
            
            return $list;
            

        }

        public function fetchCatName(){

            $this->db->select('fund_symbol, fund_name, fund_am, fund_am_symbol, fund_type');
            $query = $this->db->get('funds');

            $result = $query->result();
            $cat_arr = array();

            foreach($result as $r){
                $cat_arr[$r->fund_am_symbol][$r->fund_type][] = $r;
            }

            //var_dump($cat_arr['SCBAM']);
            //die();
            
            return $cat_arr;

        }

        public function fetchFundName(){

            /*$result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navCenterSearch3.jsp')->exec()->result();

            $doc = new DOMDocument("1.0", "utf-8");
            @$doc->loadHTML($result);
            $doc->preserveWhiteSpace = false;

            $matchName = array();

            foreach($doc->getElementsByTagName('select') as $select){

                if(preg_match("/fundName/", $select->getAttribute('name'))){
            
                    $i = 0;

                    foreach($select->getElementsByTagName('option') as $option){

                        if(preg_match("/\(([^ก-ฮ)]{2,})\)/", $option->nodeValue, $record) && $record[1] != "ห้ามขายผู้ลงทุนรายย่อย" && strpos($option->nodeValue, 'ห้ามขายผู้ลงทุนรายย่อย') == FALSE){
                                                        
                            if($record[1] == 'T3P3 (5'){
                                $matchName[$i]['fund_symbol'] = 'T3P3 (5)';
                            }
                            else if($record[1] == 'T3P3 (6'){
                                $matchName[$i]['fund_symbol'] = 'T3P3 (6)';
                            }
                            else if($record[1] == 'T3P3 (8'){
                                $matchName[$i]['fund_symbol'] = 'T3P3 (8)';
                            }
                            else if($record[1] == 'T3P3 (9'){
                                $matchName[$i]['fund_symbol'] = 'T3P3 (9)';
                            }
                            else{
                                $matchName[$i]['fund_symbol'] = $record[1];
                            }

                            $matchName[$i]['fund_name'] = $option->nodeValue;

                        }

                        $i++;

                    }

                }

            }

            var_dump($matchName);
            die();

            return $matchName;*/

            $this->db->select('fund_symbol, fund_name');
            $query = $this->db->get('funds');

            $result = $query->result();

            //var_dump($result);
            //die();
            
            return $result;
        }

        public function fetchFundHistoricPrice($symbol){
            
            $date = date("d-n-Y");

            $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=oldFund&abbrName=".$symbol."&data_month=".date('n', strtotime($date))."&data_year=2556&data_month2=".date('n', strtotime($date))."&data_year2=2561")->exec()->result();

            $match = array();//[date][assetvalue][nav][change][offer][bid][switch-offer][switch-bid][xd-date][div-date][dividends][notice]
            
            $doc = new DOMDocument("1.0", "utf-8");
            @$doc->loadHTML($result);
            $doc->preserveWhiteSpace = false;
            // grab URL and pass it to the browser
    
            foreach($doc->getElementsByTagName('table') as $table){

                if(preg_match("/#999999/", $table->getAttribute('bgcolor'))){
            
                    $i = 0;
                
                    foreach($table->getElementsByTagName('tr') as $tr){

                        $e = 0;

                        if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){

                            foreach($tr->getElementsByTagName('td') as $td){

                                //$i-2 to offset the header in the table
                                                       
                                if($e == 0){
                                    $match[$i-2]['date'] = $td->nodeValue;
                                }if($e == 1){
                                    $match[$i-2]['assetvalue'] = $td->nodeValue;
                                }if($e == 2){
                                    $match[$i-2]['nav'] = $td->nodeValue;
                                }if($e == 3){
                                    $match[$i-2]['change'] = $td->nodeValue;
                                }if($e == 4){
                                    $match[$i-2]['offer'] = $td->nodeValue;
                                }if($e == 5){
                                    $match[$i-2]['bid'] = $td->nodeValue;
                                }if($e == 6){
                                    $match[$i-2]['switch-offer'] = $td->nodeValue;
                                }if($e == 7){
                                    $match[$i-2]['switch-bid'] = $td->nodeValue;
                                }if($e == 8){
                                    $match[$i-2]['xd-date'] = $td->nodeValue;
                                }if($e == 9){
                                    $match[$i-2]['div-date'] = $td->nodeValue;
                                }if($e == 10){
                                    $match[$i-2]['dividends'] = $td->nodeValue;
                                }if($e == 11){
                                    $match[$i-2]['notice'] = $td->nodeValue;
                                }

                                $e++;

                            }
                        }

                        $i++;

                    }

                }
    
            }

            return $match;

        }
        
        public function fetchFundQuarterPrice($symbol){
            
            $date = date("d-n-Y");

            $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=oldFund&abbrName=".$symbol."&data_month=7&data_year=2561&data_month2=10&data_year2=2561")->exec()->result();

            $match = array();//[date][assetvalue][nav][change][offer][bid][switch-offer][switch-bid][xd-date][div-date][dividends][notice]
            
            $doc = new DOMDocument("1.0", "utf-8");
            @$doc->loadHTML($result);
            $doc->preserveWhiteSpace = false;
            // grab URL and pass it to the browser
    
            foreach($doc->getElementsByTagName('table') as $table){

                if(preg_match("/#999999/", $table->getAttribute('bgcolor'))){
            
                    $i = 0;
                
                    foreach($table->getElementsByTagName('tr') as $tr){

                        $e = 0;

                        if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){

                            foreach($tr->getElementsByTagName('td') as $td){

                                //$i-2 to offset the header in the table
                                                       
                                if($e == 0){
                                    $match[$i-2]['date'] = $td->nodeValue;
                                }if($e == 1){
                                    $match[$i-2]['assetvalue'] = $td->nodeValue;
                                }if($e == 2){
                                    $match[$i-2]['nav'] = $td->nodeValue;
                                }if($e == 3){
                                    $match[$i-2]['change'] = $td->nodeValue;
                                }if($e == 4){
                                    $match[$i-2]['offer'] = $td->nodeValue;
                                }if($e == 5){
                                    $match[$i-2]['bid'] = $td->nodeValue;
                                }if($e == 6){
                                    $match[$i-2]['switch-offer'] = $td->nodeValue;
                                }if($e == 7){
                                    $match[$i-2]['switch-bid'] = $td->nodeValue;
                                }if($e == 8){
                                    $match[$i-2]['xd-date'] = $td->nodeValue;
                                }if($e == 9){
                                    $match[$i-2]['div-date'] = $td->nodeValue;
                                }if($e == 10){
                                    $match[$i-2]['dividends'] = $td->nodeValue;
                                }if($e == 11){
                                    $match[$i-2]['notice'] = $td->nodeValue;
                                }

                                $e++;

                            }
                        }

                        $i++;

                    }

                }
    
            }

            return $match;

        }

        public function fetchFundHalfYearPrice($symbol){
            
            $date = date("d-n-Y");

            $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=oldFund&abbrName=".$symbol."&data_month=4&data_year=2561&data_month2=10&data_year2=2561")->exec()->result();

            $match = array();//[date][assetvalue][nav][change][offer][bid][switch-offer][switch-bid][xd-date][div-date][dividends][notice]
            
            $doc = new DOMDocument("1.0", "utf-8");
            @$doc->loadHTML($result);
            $doc->preserveWhiteSpace = false;
            // grab URL and pass it to the browser
    
            foreach($doc->getElementsByTagName('table') as $table){

                if(preg_match("/#999999/", $table->getAttribute('bgcolor'))){
            
                    $i = 0;
                
                    foreach($table->getElementsByTagName('tr') as $tr){

                        $e = 0;

                        if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){

                            foreach($tr->getElementsByTagName('td') as $td){

                                //$i-2 to offset the header in the table
                                                       
                                if($e == 0){
                                    $match[$i-2]['date'] = $td->nodeValue;
                                }if($e == 1){
                                    $match[$i-2]['assetvalue'] = $td->nodeValue;
                                }if($e == 2){
                                    $match[$i-2]['nav'] = $td->nodeValue;
                                }if($e == 3){
                                    $match[$i-2]['change'] = $td->nodeValue;
                                }if($e == 4){
                                    $match[$i-2]['offer'] = $td->nodeValue;
                                }if($e == 5){
                                    $match[$i-2]['bid'] = $td->nodeValue;
                                }if($e == 6){
                                    $match[$i-2]['switch-offer'] = $td->nodeValue;
                                }if($e == 7){
                                    $match[$i-2]['switch-bid'] = $td->nodeValue;
                                }if($e == 8){
                                    $match[$i-2]['xd-date'] = $td->nodeValue;
                                }if($e == 9){
                                    $match[$i-2]['div-date'] = $td->nodeValue;
                                }if($e == 10){
                                    $match[$i-2]['dividends'] = $td->nodeValue;
                                }if($e == 11){
                                    $match[$i-2]['notice'] = $td->nodeValue;
                                }

                                $e++;

                            }
                        }

                        $i++;

                    }

                }
    
            }

            return $match;

        }

        public function fetchFundAnnualPrice($symbol){
            
            $date = date("d-n-Y");

            $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=oldFund&abbrName=".$symbol."&data_month=".date('n', strtotime($date))."&data_year=2560&data_month2=".date('n', strtotime($date))."&data_year2=2561")->exec()->result();

            $match = array();//[date][assetvalue][nav][change][offer][bid][switch-offer][switch-bid][xd-date][div-date][dividends][notice]
            
            $doc = new DOMDocument("1.0", "utf-8");
            @$doc->loadHTML($result);
            $doc->preserveWhiteSpace = false;
            // grab URL and pass it to the browser
    
            foreach($doc->getElementsByTagName('table') as $table){

                if(preg_match("/#999999/", $table->getAttribute('bgcolor'))){
            
                    $i = 0;
                
                    foreach($table->getElementsByTagName('tr') as $tr){

                        $e = 0;

                        if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){

                            foreach($tr->getElementsByTagName('td') as $td){

                                //$i-2 to offset the header in the table
                                                       
                                if($e == 0){
                                    $match[$i-2]['date'] = $td->nodeValue;
                                }if($e == 1){
                                    $match[$i-2]['assetvalue'] = $td->nodeValue;
                                }if($e == 2){
                                    $match[$i-2]['nav'] = $td->nodeValue;
                                }if($e == 3){
                                    $match[$i-2]['change'] = $td->nodeValue;
                                }if($e == 4){
                                    $match[$i-2]['offer'] = $td->nodeValue;
                                }if($e == 5){
                                    $match[$i-2]['bid'] = $td->nodeValue;
                                }if($e == 6){
                                    $match[$i-2]['switch-offer'] = $td->nodeValue;
                                }if($e == 7){
                                    $match[$i-2]['switch-bid'] = $td->nodeValue;
                                }if($e == 8){
                                    $match[$i-2]['xd-date'] = $td->nodeValue;
                                }if($e == 9){
                                    $match[$i-2]['div-date'] = $td->nodeValue;
                                }if($e == 10){
                                    $match[$i-2]['dividends'] = $td->nodeValue;
                                }if($e == 11){
                                    $match[$i-2]['notice'] = $td->nodeValue;
                                }

                                $e++;

                            }
                        }

                        $i++;

                    }

                }
    
            }

            return $match;

        }

        public function fetchFund2YPrice($symbol){
            
            $date = date("d-n-Y");

            $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=oldFund&abbrName=".$symbol."&data_month=10&data_year=2559&data_month2=10&data_year2=2561")->exec()->result();

            $match = array();//[date][assetvalue][nav][change][offer][bid][switch-offer][switch-bid][xd-date][div-date][dividends][notice]
            
            $doc = new DOMDocument("1.0", "utf-8");
            @$doc->loadHTML($result);
            $doc->preserveWhiteSpace = false;
            // grab URL and pass it to the browser
    
            foreach($doc->getElementsByTagName('table') as $table){

                if(preg_match("/#999999/", $table->getAttribute('bgcolor'))){
            
                    $i = 0;
                
                    foreach($table->getElementsByTagName('tr') as $tr){

                        $e = 0;

                        if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){

                            foreach($tr->getElementsByTagName('td') as $td){

                                //$i-2 to offset the header in the table
                                                       
                                if($e == 0){
                                    $match[$i-2]['date'] = $td->nodeValue;
                                }if($e == 1){
                                    $match[$i-2]['assetvalue'] = $td->nodeValue;
                                }if($e == 2){
                                    $match[$i-2]['nav'] = $td->nodeValue;
                                }if($e == 3){
                                    $match[$i-2]['change'] = $td->nodeValue;
                                }if($e == 4){
                                    $match[$i-2]['offer'] = $td->nodeValue;
                                }if($e == 5){
                                    $match[$i-2]['bid'] = $td->nodeValue;
                                }if($e == 6){
                                    $match[$i-2]['switch-offer'] = $td->nodeValue;
                                }if($e == 7){
                                    $match[$i-2]['switch-bid'] = $td->nodeValue;
                                }if($e == 8){
                                    $match[$i-2]['xd-date'] = $td->nodeValue;
                                }if($e == 9){
                                    $match[$i-2]['div-date'] = $td->nodeValue;
                                }if($e == 10){
                                    $match[$i-2]['dividends'] = $td->nodeValue;
                                }if($e == 11){
                                    $match[$i-2]['notice'] = $td->nodeValue;
                                }

                                $e++;

                            }
                        }

                        $i++;

                    }

                }
    
            }

            return $match;

        }

        public function fetchFund3YPrice($symbol){
            
            $date = date("d-n-Y");

            $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=oldFund&abbrName=".$symbol."&data_month=10&data_year=2558&data_month2=10&data_year2=2561")->exec()->result();

            $match = array();//[date][assetvalue][nav][change][offer][bid][switch-offer][switch-bid][xd-date][div-date][dividends][notice]
            
            $doc = new DOMDocument("1.0", "utf-8");
            @$doc->loadHTML($result);
            $doc->preserveWhiteSpace = false;
            // grab URL and pass it to the browser
    
            foreach($doc->getElementsByTagName('table') as $table){

                if(preg_match("/#999999/", $table->getAttribute('bgcolor'))){
            
                    $i = 0;
                
                    foreach($table->getElementsByTagName('tr') as $tr){

                        $e = 0;

                        if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){

                            foreach($tr->getElementsByTagName('td') as $td){

                                //$i-2 to offset the header in the table
                                                       
                                if($e == 0){
                                    $match[$i-2]['date'] = $td->nodeValue;
                                }if($e == 1){
                                    $match[$i-2]['assetvalue'] = $td->nodeValue;
                                }if($e == 2){
                                    $match[$i-2]['nav'] = $td->nodeValue;
                                }if($e == 3){
                                    $match[$i-2]['change'] = $td->nodeValue;
                                }if($e == 4){
                                    $match[$i-2]['offer'] = $td->nodeValue;
                                }if($e == 5){
                                    $match[$i-2]['bid'] = $td->nodeValue;
                                }if($e == 6){
                                    $match[$i-2]['switch-offer'] = $td->nodeValue;
                                }if($e == 7){
                                    $match[$i-2]['switch-bid'] = $td->nodeValue;
                                }if($e == 8){
                                    $match[$i-2]['xd-date'] = $td->nodeValue;
                                }if($e == 9){
                                    $match[$i-2]['div-date'] = $td->nodeValue;
                                }if($e == 10){
                                    $match[$i-2]['dividends'] = $td->nodeValue;
                                }if($e == 11){
                                    $match[$i-2]['notice'] = $td->nodeValue;
                                }

                                $e++;

                            }
                        }

                        $i++;

                    }

                }
    
            }

            return $match;

        }

        public function fetchFundPresentPrice($symbol){
            
            $date = date("d-n-Y");

            $hit = false;
            $date = date('d-m-Y');
            //$month = date('m');
            //$year = date('Y')+543;
            $doc = new DOMDocument("1.0", "utf-8");

            //var_dump(date('m', strtotime($date)));
            //die();

            $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=abbr_name&fundupradio=radiovall&fundupmullist2=".$symbol."&data_date=".(string)date('d', strtotime($date))."&data_month=".(string)date('m', strtotime($date))."&data_year=".(string)(date('Y', strtotime($date))+543))->exec()->result();
            while($hit == false){
                               
                @$doc->loadHTML($result);
                $doc->preserveWhiteSpace = false;

                if($doc->getElementsByTagName('span')[1]->textContent == 'ไม่พบข้อมูลในฐานข้อมูล'){

                    $date = date('d-m-Y', strtotime('-1 day', strtotime($date)));
                    //var_dump($date);
                    //die();
                    //date_sub($date, date_interval_create_from_date_string('1 day'));
                    $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                        ->POST("searchType=abbr_name&fundupradio=radiovall&fundupmullist2=".$symbol."&data_date=".(string)date('d', strtotime($date))."&data_month=".(string)date('m', strtotime($date))."&data_year=".(string)(date('Y', strtotime($date))+543))->exec()->result();
                
                }
                else{
                    $hit = true;
                }
                
            }
            $match = array();
            
            //$doc = new DOMDocument("1.0", "utf-8");
            //@$doc->loadHTML($result);
            //$doc->preserveWhiteSpace = false;

            //var_dump($doc->getElementsByTagName('span')[1]);
            //die();

            foreach($doc->getElementsByTagName('tr') as $tr){
                $e = 0;
                if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){
                    foreach($tr->getElementsByTagName('td') as $td){
                        
                        if($e == 0){
                            $match['fund_name'] = $td->nodeValue;
                        }if($e == 1){
                            $match['assetvalue'] = $td->nodeValue;
                        }if($e == 2){
                            $match['nav'] = $td->nodeValue;
                        }if($e == 3){
                            $match['change'] = $td->nodeValue;
                        }if($e == 4){
                            $match['offer'] = $td->nodeValue;
                        }if($e == 5){
                            $match['bid'] = $td->nodeValue;
                        }if($e == 6){
                            $match['switch-offer'] = $td->nodeValue;
                        }if($e == 7){
                            $match['switch-bid'] = $td->nodeValue;
                        }if($e == 8){
                            $match['xd-date'] = $td->nodeValue;
                        }if($e == 9){
                            $match['div-date'] = $td->nodeValue;
                        }if($e == 10){
                            $match['dividends'] = $td->nodeValue;
                        }if($e == 11){
                            $match['notice'] = $td->nodeValue;
                        }
                        
                        $e++;
                      
                    }
                }
            }

            

            $match['fund_symbol'] = $symbol;
            
            $type = $doc->getElementsByTagName('b');

            if($type[0] == NULL){ 
                $match['fund_type'] = NULL; 
            }
            else{ 
                $match['fund_type'] = $type[0]->textContent;
            }
            $am = $doc->getElementsByTagName('span');
            $match['fund_am'] = $am[1]->textContent;

            $match['date'] = $date;
            return $match;
            
            

        }

        public function fetchMonthlyChange($symbol, $m){

            $date = date("d-n-Y");

            $change_arr = array();

            do{
                $date = date('d-n-Y', strtotime('-1 month', strtotime($date)));

                //var_dump($date);
                //die();

                $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=oldFund&abbrName=".$symbol."&data_month=".date('n', strtotime($date))."&data_year=".(date('Y', strtotime($date))+543)."&data_month2=".date('n', strtotime($date))."&data_year2=".(date('Y', strtotime($date))+543))->exec()->result();

                $match = array();//[date][assetvalue][nav][change][offer][bid][switch-offer][switch-bid][xd-date][div-date][dividends][notice]
            
                $doc = new DOMDocument("1.0", "utf-8");
                @$doc->loadHTML($result);
                $doc->preserveWhiteSpace = false;
                // grab URL and pass it to the browser

                if($doc->getElementsByTagName('span')[1]->textContent == 'ไม่พบข้อมูลในฐานข้อมูล'){
                    $change_arr[$m][] = null;
                    $change_arr[$m][] = null;
                    $m++;
                }
                else{
                foreach($doc->getElementsByTagName('table') as $table){

                    if(preg_match("/#999999/", $table->getAttribute('bgcolor'))){
            
                        $i = 0;
                
                        foreach($table->getElementsByTagName('tr') as $tr){

                            $e = 0;

                            if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){

                                foreach($tr->getElementsByTagName('td') as $td){

                                    //$i-2 to offset the header in the table
                                                       
                                    if($e == 0){
                                        $match[$i-2]['date'] = $td->nodeValue;
                                    }if($e == 1){
                                        $match[$i-2]['assetvalue'] = $td->nodeValue;
                                    }if($e == 2){
                                        $match[$i-2]['nav'] = $td->nodeValue;
                                    }if($e == 3){
                                        $match[$i-2]['change'] = $td->nodeValue;
                                    }if($e == 4){
                                        $match[$i-2]['offer'] = $td->nodeValue;
                                    }if($e == 5){
                                        $match[$i-2]['bid'] = $td->nodeValue;
                                    }if($e == 6){
                                        $match[$i-2]['switch-offer'] = $td->nodeValue;
                                    }if($e == 7){
                                        $match[$i-2]['switch-bid'] = $td->nodeValue;
                                    }if($e == 8){
                                        $match[$i-2]['xd-date'] = $td->nodeValue;
                                    }if($e == 9){
                                        $match[$i-2]['div-date'] = $td->nodeValue;
                                    }if($e == 10){
                                        $match[$i-2]['dividends'] = $td->nodeValue;
                                    }if($e == 11){
                                        $match[$i-2]['notice'] = $td->nodeValue;
                                    }

                                    $e++;

                                }
                            }

                            $i++;

                        }

                    }
    
                }

                $change_arr[$m][] = $match[0]['nav'];
                $change_arr[$m][] = end($match)['nav'];

                $m++;
            }
            }
            while($m <= 12);

            //var_dump($change_arr);

            return $change_arr;

        }
        public function fetchYearlyChange($symbol){

            $date = date("d-n-Y");

            $change_arr = array();

            $m = 1;

            do{
                $date = date('d-n-Y', strtotime('-1 year', strtotime($date)));

                //var_dump($date);
                //die();

                $result = $this->Curl->url('http://www.thaimutualfund.com/AIMC/aimc_navSearchResult.jsp')
                                ->POST("searchType=oldFund&abbrName=".$symbol."&data_month=12&data_year=".(date('Y', strtotime($date))+542)."&data_month2=12&data_year2=".(date('Y', strtotime($date))+543))->exec()->result();

                $match = array();//[date][assetvalue][nav][change][offer][bid][switch-offer][switch-bid][xd-date][div-date][dividends][notice]
            
                $doc = new DOMDocument("1.0", "utf-8");
                @$doc->loadHTML($result);
                $doc->preserveWhiteSpace = false;
                // grab URL and pass it to the browser

                if($doc->getElementsByTagName('span')[1]->textContent == 'ไม่พบข้อมูลในฐานข้อมูล'){
                    break;
                }
    
                foreach($doc->getElementsByTagName('table') as $table){

                    if(preg_match("/#999999/", $table->getAttribute('bgcolor'))){
            
                        $i = 0;
                
                        foreach($table->getElementsByTagName('tr') as $tr){

                            $e = 0;

                            if(preg_match("/#F2F2F2/", $tr->getAttribute('bgcolor'))){

                                foreach($tr->getElementsByTagName('td') as $td){

                                    //$i-2 to offset the header in the table
                                                       
                                    if($e == 0){
                                        $match[$i-2]['date'] = $td->nodeValue;
                                    }if($e == 1){
                                        $match[$i-2]['assetvalue'] = $td->nodeValue;
                                    }if($e == 2){
                                        $match[$i-2]['nav'] = $td->nodeValue;
                                    }if($e == 3){
                                        $match[$i-2]['change'] = $td->nodeValue;
                                    }if($e == 4){
                                        $match[$i-2]['offer'] = $td->nodeValue;
                                    }if($e == 5){
                                        $match[$i-2]['bid'] = $td->nodeValue;
                                    }if($e == 6){
                                        $match[$i-2]['switch-offer'] = $td->nodeValue;
                                    }if($e == 7){
                                        $match[$i-2]['switch-bid'] = $td->nodeValue;
                                    }if($e == 8){
                                        $match[$i-2]['xd-date'] = $td->nodeValue;
                                    }if($e == 9){
                                        $match[$i-2]['div-date'] = $td->nodeValue;
                                    }if($e == 10){
                                        $match[$i-2]['dividends'] = $td->nodeValue;
                                    }if($e == 11){
                                        $match[$i-2]['notice'] = $td->nodeValue;
                                    }

                                    $e++;

                                }
                            }

                            $i++;

                        }

                    }
    
                }

                $change_arr[$m][] = $match[0]['nav'];
                $change_arr[$m][] = end($match)['nav'];

                $m++;
            }
            while($m <= 5);

            return $change_arr;

        }
    }

?>