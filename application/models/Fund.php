<?php

Class Fund extends CI_Model{

    private $fund_id;
    private $fund_symbol;
    private $fund_name;
    private $fund_am_symbol;
    private $fund_am;
    private $fund_type;
    private $fund_aar;
    private $fund_risk;

    public function __construct($data = array()){
        parent::__construct($data);
        $this->load->database();
    }

    public function getById($id){
        if($id == NULL){
            return false;
        }

        $this->db->from('funds');
        $this->db->where('fund_id',$id);
        
        $execute = $this->db->get();  
        $fund = $execute->row(); 

        if($fund == true){
            return $fund;
        }else{
            return false;
        }
    }

    public function getBySymbol($symbol){
        if($symbol == NULL){
            return false;
        }

        $this->db->from('funds');
        $this->db->where('fund_symbol',$symbol);
        
        $execute = $this->db->get();  
        $fund = $execute->row(); 

        if($fund == true){
            return $fund;
        }else{
            return false;
        }
    }

    public function getAMSymbol($symbol){
        $this->db->select('fund_am_symbol');
        $this->db->from('funds');
        $this->db->where('fund_symbol', $symbol);

        $execute = $this->db->get();
        return $execute->row()->fund_am_symbol;
    }

    public function add($data){

        $whitelist = array(
                'fund_symbol',
                'fund_name',
                'fund_am',
                'fund_type'
        );

        foreach($whitelist as $index){
            if($data['fund_am'] == 'ไม่พบข้อมูลในฐานข้อมูล'){
                return;
            }
            if(isset($data[$index])){
                $this->db->set($index,$data[$index]);
            }else{
                throw new Exception('require '.$index.' at '.$data['fund_symbol']);
            }
        }

        $this->db->insert('funds');

        $fund = new Fund($data);
        $fund->fund_id = $this->db->insert_id();

        return $this;
    }


}

?>