<?php

Class User extends CI_Model{

    private $user_id;
    private $username;
    private $password;
    private $email;

    public function __construct(){
        $this->load->database();
    }

    public function register($data){
        $this->db->where('username', $data['username']);
        if($this->db->count_all_results('users') > 0){
            throw new Exception('ชื่อผู้ใช้นี้มีการใช้งานแล้ว');
        }

        $this->username = $data['username'];
        $this->password = $data['password'];
        $this->email = $data['email'];
        
        $this->db->set('username', $data['username']);
        $this->db->set('password', $data['password']);
        $this->db->set('email', $data['email']);

        $this->db->insert('users');

        $this->user_id = $this->db->insert_id();

        return;
    }

    public function login($data){

        $this->db->where('username', $data['username']);
        if($this->db->count_all_results('users') == 1){
            $this->db->where('password', $data['password']);
            if($this->db->count_all_results('users')){
                $this->db->select('user_id');
                $this->db->from('users');
                $this->db->where('username', $data['username']);
                $this->db->where('password', $data['password']);
                $query = $this->db->get()->result();

                return $query[0];
            }
            else{
                throw new Exception('User not found');
            }
        }

    }

    public function login_validation($username, $password){
        
        $this->db->where('username', $username);
        
        if($this->db->count_all_results('users')){
            $this->db->select('password');
            $this->db->from('users');
            $this->db->where('username', $username);

            $query = $this->db->get()->result();

            if($query[0]->password == $password){
                return true;
            }
            else{
                return false;
            }
        }

    }

    public function uname_available($uname){
        
        $this->db->where('username', $uname);
        
        if($this->db->count_all_results('users')){
            return false;
        }
        else return true;

    }

    public function email_available($email){
        
        $this->db->where('email', $email);
        
        if($this->db->count_all_results('users')){
            return false;
        }
        else return true;

    }



}

?>