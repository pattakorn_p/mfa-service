<?php
Class Curl extends CI_Model{
    
    private $curlObj;
    private $curlResult;

    public function __construct(){

        $this->curlObj = curl_init();
        
		curl_setopt($this->curlObj, CURLOPT_RETURNTRANSFER, 1);
        @curl_setopt($this->curlObj, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($this->curlObj, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
    }

    public function url($url){
        curl_setopt($this->curlObj, CURLOPT_URL, $url);
        return $this;
    }

    public function info(){
       return curl_getinfo($this->curlObj);
    }

    public function POST($data){
        curl_setopt($this->curlObj, CURLOPT_POST, 1 );
        curl_setopt($this->curlObj, CURLOPT_POSTFIELDS,$data);		
        return $this;
    }

    public function header(){
        curl_setopt($this->curlObj, CURLOPT_HEADER, 1);
        return $this;
    }

    public function referer($ref){
        curl_setopt($this->curlObj, CURLOPT_REFERER, $ref);
        return $this;
    }

    public function exec(){

        $this->curlResult = curl_exec($this->curlObj);
    
        if($this->curlResult === false){
            
            return $this;
        }
        
        return $this;
    }
    
    public function result(){
        return $this ->curlResult;
    }
    
    public function __destruct(){
         curl_close($this->curlObj);
    }
}

?>