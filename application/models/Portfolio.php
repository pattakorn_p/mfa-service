<?php

Class Portfolio extends CI_Model{

    private $user_id;
    private $fund_id;
    private $amount;
    private $avg_cost;

    public function __construct(){
        $this->load->database();
    }

    public function addEntry($data){
        //Date should be added as well
        
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('fund_id', $data['fund_id']);

        if($this->db->count_all_results('portfolio_entry') > 0){

            $this->db->select('amount, average_cost');
            $this->db->where('user_id', $data['user_id']);
            $this->db->where('fund_id', $data['fund_id']);

            $result = $this->db->get('portfolio_entry');

            $pre_amount = $result->result()[0]->amount;
            $pre_cost = $result->result()[0]->average_cost;

            $new_amount = $data['amount'] + (int)$result->result()[0]->amount;
            
            $new_cost = ($data['average_cost'] + (int)$result->result()[0]->average_cost)/2;

            $this->db->set('amount', $new_amount);
            $this->db->set('average_cost', $new_cost);
            
            $this->db->where('user_id', $data['user_id']);
            $this->db->where('fund_id', $data['fund_id']);

            $this->db->update('portfolio_entry');
        }
        else{
        
            $whitelist = array(
                'user_id',
                'fund_id',
                'amount',
                'average_cost'
            );

            foreach($whitelist as $index){
                if(isset($data[$index])){
                    $this->db->set($index,$data[$index]);
                }else{
                    throw new Exception('require '.$index);
                }
            }
            
            $this->db->insert('portfolio_entry');
        }


    }

    public function addFavEntry($data){
        //Date should be added as well
        
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('fund_id', $data['fund_id']);

        if($this->db->count_all_results('favorite_entry') > 0){
            return;
        }
        $whitelist = array(
            'user_id',
            'fund_id'
        );

        foreach($whitelist as $index){
            if(isset($data[$index])){
                $this->db->set($index,$data[$index]);
            }else{
                throw new Exception('require '.$index);
            }
        }
            
            $this->db->insert('favorite_entry');
        


    }

    public function editEntry($data){
        
        $this->db->set('amount', $data['amount']);
        $this->db->set('average_cost', $data['average_cost']);


        $this->db->where('user_id', $data['user_id']);
        $this->db->where('fund_id', $data['fund_id']);
        
        $this->db->update('portfolio_entry');

    }

    public function delEntry($data){


        $this->db->where('user_id', $data['user_id']);
        $this->db->where('fund_id', $data['fund_id']);
        
        $this->db->delete('portfolio_entry');

    }

    public function delFavEntry($data){


        $this->db->where('user_id', $data['user_id']);
        $this->db->where('fund_id', $data['fund_id']);
        
        $this->db->delete('favorite_entry');

    }

    public function getPortByUser($id){
        $port = array();

        $this->db->from('portfolio_entry');
        $this->db->where('user_id', $id);

        $execute = $this->db->get();
        foreach($execute->result() as $e){
            $port[] = [
                'user_id' => $e->user_id,
                'fund_id' => $e->fund_id,
                'amount' => $e->amount,
                'average_cost' => $e->average_cost
            ];
        }
        return $port;

    }

    public function getFavByUser($id){
        $fav = array();

        $this->db->from('favorite_entry');
        $this->db->where('user_id', $id);

        $execute = $this->db->get();
        foreach($execute->result() as $e){
            $fav[] = [
                'user_id' => $e->user_id,
                'fund_id' => $e->fund_id
            ];
        }
        return $fav;

    }



}

?>