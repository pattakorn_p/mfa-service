<?php

    Class Stats{

        public function __construct(){
            $this->ci = get_instance();
            $this->ci->load->model('Fetcher');
            $this->ci->load->database();
        }

        public function getAnnualChange($symbol){

            $data = $this->ci->Fetcher->fetchFundAnnualPrice($symbol);

            $start_nav = (float)$data[0]['nav'];
            
            $end_nav = (float)end($data)['nav'];

            $nav_change = (($start_nav - $end_nav)/$end_nav);

            return $nav_change;

        }
        public function get2YChange($symbol){
            
            $data = $this->ci->Fetcher->fetchFund2YPrice($symbol);

            $start_nav = (float)$data[0]['nav'];
            
            $end_nav = (float)end($data)['nav'];
            
            $nav_change = (($start_nav - $end_nav)/$end_nav);

            return $nav_change;
        }

        public function get3YChange($symbol){
            
            $data = $this->ci->Fetcher->fetchFund3YPrice($symbol);

            $start_nav = (float)$data[0]['nav'];
            
            $end_nav = (float)end($data)['nav'];
            
            $nav_change = (($start_nav - $end_nav)/$end_nav);

            return $nav_change;
        }

        public function getQuarterChange($symbol){
            $data = $this->ci->Fetcher->fetchFundQuarterPrice($symbol);

            $start_nav = (float)$data[0]['nav'];
            
            $end_nav = (float)end($data)['nav'];
            
            $nav_change = (($start_nav - $end_nav)/$end_nav);

            return $nav_change;
            
        }

        public function getHalfYearChange($symbol){
            $data = $this->ci->Fetcher->fetchFundHalfYearPrice($symbol);

            $start_nav = (float)$data[0]['nav'];
            
            $end_nav = (float)end($data)['nav'];
            
            $nav_change = (($start_nav - $end_nav)/$end_nav);

            return $nav_change;
            
        }

        public function perfCompare($duration, $initial, $funds){

            $result = array();

            foreach($funds as $f){
                
                //var_dump($f);
                if($duration == 3){
                    $change = $this->getQuarterChange($f);
                    $result[$f] = ['change' => $change,
                                   'return' => $initial * $change,
                                   'result' => $initial + ($initial * $change)];
                }
                
                else if($duration == 6){
                    $change = $this->getHalfYearChange($f);
                    $result[$f] = ['change' => $change,
                                   'return' => $initial * $change,
                                   'result' => $initial + ($initial * $change)];
                }

                else if($duration == 12){
                    $change = $this->getAnnualChange($f);
                    $result[$f] = ['change' => $change,
                                   'return' => $initial * $change,
                                   'result' => $initial + ($initial * $change)];
                }

                else if($duration == 24){
                    $change = $this->get2YChange($f);
                    $result[$f] = ['change' => $change,
                                   'return' => $initial * $change,
                                   'result' => $initial + ($initial * $change)];
                }

                else if($duration == 36){
                    $change = $this->get3YChange($f);
                    $result[$f] = ['change' => $change,
                                   'return' => $initial * $change,
                                   'result' => $initial + ($initial * $change)];
                }

            }

            return $result;
            
        }

        public function predict($symbol){
            $data = $this->ci->Fetcher->fetchMonthlyChange($symbol, 1);
            $i = 0;

            //var_dump($data);
            //die();
            $data = array_reverse($data);

            $ma_arr = array();
            $his_arr = array();


            foreach($data as $d){
                if(is_numeric($d[0]) && is_numeric($d[1])){
                    $ma_arr[] = round((($d[0] - $d[1]) / $d[1]) * 100, 4);
                }
                else{
                    $ma_arr[] = 0;
                }
            }


            while($i < 12){

                $result = round((array_sum($ma_arr))/12, 4);

                array_push($ma_arr, $result);
                $his_arr[] = array_shift($ma_arr);

                $i++;
            }

            $price_arr['historic'] = $his_arr;
            $price_arr['forecast'] = $ma_arr;

            return $price_arr;
        }

        public function planner($init, $target, $duration, $risk, $am){

            $incr = (($target - $init)/$init) * 100;

            $yearly_incr = $incr/$duration;

            if($risk == 1){//MMF = 30%, FIF = 40%, EQ = 30%

                $mmf = $incr*0.3;
                $fif = $incr*0.4;
                $eq = $incr*0.3;
                
                $mmf_per_fund = $mmf/2;//2*15%
                $fif_per_fund = $fif/3;//2*15%|1*10%
                $eq_per_fund = $eq/2;//2*15%


                //Finding MMF funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตลาดเงิน');
                $this->ci->db->where('fund_aar >=', $mmf_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(2);
                $this->ci->db->from('funds');

                $mmf_q = $this->ci->db->get();
                $result['mmf'] = $mmf_q->result();
                
                $threshold = $mmf_per_fund/$duration;

                while(count($mmf_q->result()) < 2){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตลาดเงิน');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(2);
                    $this->ci->db->from('funds');

                    $mmf_q = $this->ci->db->get();
                    $result['mmf'] = $mmf_q->result();
                }
                //$sql = "SELECT *  FROM `funds` WHERE `fund_am_symbol` LIKE \'TMBAM\' AND `fund_type` LIKE \'กองทุนรวมตราสารหนี้%\' AND `fund_aar` >= 1.409 ORDER BY `fund_aar` ASC";

                //Finding FIF Funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตราสารหนี้');
                $this->ci->db->where('fund_aar >=', $fif_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(3);
                $this->ci->db->from('funds');

                $fif_q = $this->ci->db->get();
                $result['fif'] = $fif_q->result();
                
                $threshold = $fif_per_fund/$duration;

                while(count($fif_q->result()) < 3){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตราสารหนี้');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(3);
                    $this->ci->db->from('funds');

                    $fif_q = $this->ci->db->get();
                    $result['fif'] = $fif_q->result();
                }

                //Finding EQ Funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตราสารทุน');
                $this->ci->db->where('fund_aar >=', $eq_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(2);
                $this->ci->db->from('funds');

                $eq_q = $this->ci->db->get();
                $result['eq'] = $eq_q->result();

                $threshold = $eq_per_fund/$duration;

                while(count($eq_q->result()) < 2){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตราสารทุน');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(2);
                    $this->ci->db->from('funds');

                    $eq_q = $this->ci->db->get();
                    $result['eq'] = $eq_q->result();
                }
                $result['ratio'] = [15, 15, 15, 15, 10, 15, 15];

                return $result;

            }
            else if($risk == 2){//MMF = 20%, FIF = 30%, EQ = 50%
                
                $mmf = $incr*0.2;
                $fif = $incr*0.3;
                $eq = $incr*0.5;
                
                $mmf_per_fund = $mmf/2;//2*10%
                $fif_per_fund = $fif/2;//2*15%
                $eq_per_fund = $eq/2;//2*25%


                //Finding MMF funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตลาดเงิน');
                $this->ci->db->where('fund_aar >=', $mmf_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(2);
                $this->ci->db->from('funds');

                $mmf_q = $this->ci->db->get();
                $result['mmf'] = $mmf_q->result();
                
                $threshold = $mmf_per_fund/$duration;

                while(count($mmf_q->result()) < 2){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตลาดเงิน');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(2);
                    $this->ci->db->from('funds');

                    $mmf_q = $this->ci->db->get();
                    $result['mmf'] = $mmf_q->result();
                }
                //$sql = "SELECT *  FROM `funds` WHERE `fund_am_symbol` LIKE \'TMBAM\' AND `fund_type` LIKE \'กองทุนรวมตราสารหนี้%\' AND `fund_aar` >= 1.409 ORDER BY `fund_aar` ASC";

                //Finding FIF Funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตราสารหนี้');
                $this->ci->db->where('fund_aar >=', $fif_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(2);
                $this->ci->db->from('funds');

                $fif_q = $this->ci->db->get();
                $result['fif'] = $fif_q->result();
                
                $threshold = $fif_per_fund/$duration;

                while(count($fif_q->result()) < 2){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตราสารหนี้');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(2);
                    $this->ci->db->from('funds');

                    $fif_q = $this->ci->db->get();
                    $result['fif'] = $fif_q->result();
                }

                //Finding EQ Funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตราสารทุน');
                $this->ci->db->where('fund_aar >=', $eq_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(2);
                $this->ci->db->from('funds');

                $eq_q = $this->ci->db->get();
                $result['eq'] = $eq_q->result();

                $threshold = $eq_per_fund/$duration;

                while(count($eq_q->result()) < 2){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตราสารทุน');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(2);
                    $this->ci->db->from('funds');

                    $eq_q = $this->ci->db->get();
                    $result['eq'] = $eq_q->result();
                }
                $result['ratio'] = [10, 10, 15, 15, 25, 25];

                return $result;


            }
            else if($risk == 3){//MMF = 10%, FIF = 20%, EQ = 70%
                
                $mmf = $incr*0.1;
                $fif = $incr*0.2;
                $eq = $incr*0.7;
                
                $mmf_per_fund = $mmf/1;//1*10%
                $fif_per_fund = $fif/2;//2*10%
                $eq_per_fund = $eq/2;//2*35%


                //Finding MMF funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตลาดเงิน');
                $this->ci->db->where('fund_aar >=', $mmf_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(1);
                $this->ci->db->from('funds');

                $mmf_q = $this->ci->db->get();
                $result['mmf'] = $mmf_q->result();

                $threshold = $mmf_per_fund/$duration;

                while(count($mmf_q->result()) < 1){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตลาดเงิน');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(1);
                    $this->ci->db->from('funds');

                    $mmf_q = $this->ci->db->get();
                    $result['mmf'] = $mmf_q->result();
                }
                //$sql = "SELECT *  FROM `funds` WHERE `fund_am_symbol` LIKE \'TMBAM\' AND `fund_type` LIKE \'กองทุนรวมตราสารหนี้%\' AND `fund_aar` >= 1.409 ORDER BY `fund_aar` ASC";

                //Finding FIF Funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตราสารหนี้');
                $this->ci->db->where('fund_aar >=', $fif_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(2);
                $this->ci->db->from('funds');

                $fif_q = $this->ci->db->get();
                $result['fif'] = $fif_q->result();
                
                $threshold = $fif_per_fund/$duration;

                while(count($fif_q->result()) < 2){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตราสารหนี้');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(2);
                    $this->ci->db->from('funds');

                    $fif_q = $this->ci->db->get();
                    $result['fif'] = $fif_q->result();
                }

                //Finding EQ Funds
                $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                $this->ci->db->where_in('fund_am_symbol', $am);
                $this->ci->db->like('fund_type', 'กองทุนรวมตราสารทุน');
                $this->ci->db->where('fund_aar >=', $eq_per_fund/$duration);
                $this->ci->db->order_by('fund_aar', 'DESC');
                $this->ci->db->limit(2);
                $this->ci->db->from('funds');

                $eq_q = $this->ci->db->get();
                $result['eq'] = $eq_q->result();
                $threshold = $eq_per_fund/$duration;

                while(count($eq_q->result()) < 2){
                    $threshold = $threshold*0.75;
                    $this->ci->db->select('fund_symbol, fund_name, fund_aar, fund_am_symbol');
                    $this->ci->db->where_in('fund_am_symbol', $am);
                    $this->ci->db->like('fund_type', 'กองทุนรวมตราสารทุน');
                    $this->ci->db->where('fund_aar >=', $threshold);
                    $this->ci->db->order_by('fund_aar', 'DESC');
                    $this->ci->db->limit(2);
                    $this->ci->db->from('funds');

                    $eq_q = $this->ci->db->get();
                    $result['eq'] = $eq_q->result();
                }
                $result['ratio'] = [10, 10, 10, 35, 35];

                return $result;


            }
            
            
            //var_dump($incr);
            //die();
        }

        public function lowRiskFundSearch($am, $yearly_incr){

            /*
            select * from table 
            where value >= ($myvalue * .9) and value <= ($myvalue * 1.1) 
            order by abs(value - $myvalue) limit 1
            */



        }

        public function getAAR($symbol){
            
            $data = $this->ci->Fetcher->fetchYearlyChange($symbol);
            $acc = array();

            foreach($data as $d){
                var_dump($d[0]);
                var_dump($d[1]);

                if(!is_numeric($d[0]) || !is_numeric($d[1])){
                    continue;
                }

                else if($d[0] == 0 || $d[1] == 0){
                    continue;
                }

                $acc[] = (((float)$d[0]-(float)$d[1])/(float)$d[1])*100.00;
            }

            $acc = array_sum($acc)/count($acc);

            if(is_nan($acc)){
                return 0.00;
            }
            else{
                return $acc;
            }
        }

        public function insertAAR(){

            //$this->ci->load->database();

            $name = $this->ci->Fetcher->fetchFundName();

            foreach($name as $n){

                $aar = $this->getAAR($n->fund_symbol);
                var_dump($n);
                echo "<br>";

                $this->ci->db->set('fund_aar', $aar);
                $this->ci->db->where('fund_symbol', $n->fund_symbol);
                $this->ci->db->update('funds');
                //break;
            }


        }
    }

?>