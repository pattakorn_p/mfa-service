<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['view/index'] = 'REST/View_controller/index';
$route['name'] = 'REST/Fund_controller/fetchFundName';
$route['cat'] = 'REST/Fund_controller/fetchCatName';
$route['price/(:any)'] = 'REST/Fund_controller/fetchFundPrice/$1';
$route['info/(:any)'] = 'REST/Fund_controller/fetchFundInfo/$1';
$route['am/(:any)'] = 'REST/Fund_controller/fetchByAM/$1';

$route['addName'] = 'REST/Fund_controller/addFundName';
$route['view/detail/(:any)'] = 'REST/View_controller/detail/$1';

$route['register'] = 'REST/User_controller/register';
$route['login'] = 'REST/User_controller/login';
$route['logout'] = 'REST/User_controller/logout';


$route['view/port'] = 'REST/View_controller/port/';
$route['port_get'] = 'REST/Port_controller/getPortByUser';
$route['port_add'] = 'REST/Port_controller/addEntry';
$route['port_edit'] = 'REST/Port_controller/editEntry';
$route['port_del'] = 'REST/Port_controller/delEntry';
$route['fav_add'] = 'REST/Port_controller/addFavEntry';
$route['fav_del'] = 'REST/Port_controller/delFavEntry';

$route['view/favor'] = 'REST/View_controller/favorite';

$route['view/compare'] = 'REST/View_controller/compare/';
$route['view/compared'] = 'REST/View_controller/compared/';

$route['view/fundlist/(:any)'] = 'REST/View_controller/fundlist/$1';

$route['compare'] = 'REST/Stat_controller/perfCompare/';

$route['q_change/(:any)'] = 'REST/Stat_controller/getQuarterChange/$1';

$route['h_change/(:any)'] = 'REST/Stat_controller/getHalfYearChange/$1';

$route['a_change/(:any)'] = 'REST/Stat_controller/getAnnualChange/$1';

$route['2y_change/(:any)'] = 'REST/Stat_controller/get2YChange/$1';

$route['3y_change/(:any)'] = 'REST/Stat_controller/get3YChange/$1';

$route['predict/(:any)'] = 'REST/Stat_controller/predict/$1';

$route['view/predict_form'] = 'REST/View_controller/prediction';

$route['view/predicted'] = 'REST/View_controller/predicted';

$route['plan'] = 'REST/Stat_controller/planner';

$route['aar'] = 'REST/Stat_controller/insertAAR';
$route['view/plan_form'] = 'REST/View_controller/planning';

$route['view/plan_result'] = 'REST/View_controller/planned';

$route['view/login_validation'] = 'REST/User_controller/login_validation';
$route['view/uname_available'] = 'REST/User_controller/uname_available';
$route['view/email_available'] = 'REST/User_controller/email_available';
